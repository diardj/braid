#!/usr/bin/env python

from setuptools import setup

rsrc="../resources/"
setup(name='BRAID',
      version='0.0.1',
      description='BRAID Model Software',
      author="Diard, Valdois, Phénix, Ginestet, Saghiran, Steinhilber",
      author_email="julien.diard@univ-grenoble-alpes.fr",
      url="https://gricad-gitlab.univ-grenoble-alpes.fr/diardj/braid",
      #scripts=['braid'],
      packages=['braidpy'],
      package_dir={'braidpy' :'braidpy'},
      package_data= {'braidpy':[rsrc+"chardicts/*", rsrc+'confusionMatrix/*', rsrc+"lexicon/*.csv"]}
     )
