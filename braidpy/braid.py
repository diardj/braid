#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
braid.py:  [a file of the BRAID software] BRAID model class."""
__copyright__ = "2021, CNRS, UGA"
__authors__ = "Diard, Valdois, Phénix, Ginestet, Saghiran, Steinhilber"
__contact__ = "Julien Diard, julien.diard@univ-grenoble-alpes.fr"
__licence__ = "CECILL-2.1"
__date__ = "Aug 23 2021"

# General purpose libraries
import sys, logging

# Scientific/Numerical computing
from numpy import linalg as LA
import numpy as np

# BRAID utlities
import braidpy.modality as mod
import braidpy.lexicon as lxc


class braid:
    u"""The braid class corresponds to the computational model. In normal use, it is declared inside a simulation context (see class simu) and its state (probability distributions) evolves during iterations.

        Attributes:
            langue (str): lexicon language (added to allow a non expert manipulation of the model)
            lexicon_name (str): lexicon filename
            lexicon (pandas.DataFrame): lexicon table
            freq: word frequencies (only words with the chosen length)
            dist_names (list): names of probability distributions.
            shared_parameters (list): names of attributes shared with :obj:`modality.modality` and :obj:`simu.simu` classes
            ortho (:obj:`modality.orthography`): orthographical component
            phono (:obj:`modality.phonology`): phonological component
            meanA (float): visual attention focus :math:`\\mu_A`
            gazePosition (float): gaze position :math:`G`
            sdA (float): visual attention dispersion :math:`\\sigma_A`
            sdM (float): maximal value for visual attention dispersion
            leakW (float): parameter of the information decay at the word level :math:`\\textit{Leak}_W`
            weightLDYES (float): parameter of the information decay in the lexical membership submodel :math:`\\textit{Leak}_{LD}`
                (/!\ This parameter concerns only the YES answer, but it supposed equal to the one for the NO answer - `weightLDNO`)
            ld_trans_mat (numpy.ndarray): transition matrix expressing information decay in the model the lexical membership submodel
            version (int): frequency table use and Lexical decision computing method
            reset (Dict): elements to reset when calling the method ``self.reset_model()``
    """

    def __init__(self, stim="", langue="", lexicon_name="",
                 conf_mat_name="", ortho_char_name="", phono_char_name="", enable_phono=False, store_ortho=True, store_phono=True,
                 remove_stim_ortho=False, remove_stim_phono=False, top_down_influence=True, top_down_influence_phono=False, version=7):
        u"""Object constructor, braid model initiator

            Args:
                stim (str): character string to read/recognize
                langue (str): lexicon language "en", "fr" or "ge" (/!\if langue is specified, do not specify lexicon_name and *vice versa*)
                lexicon_name (str): lexicon filename (see ``resources/lexicon`` directory)
                conf_mat_name (str): the letter confusion matrix filename (see ``resources/confusionMatrix`` directory)
                ortho_char_name (str): the orthographic characters filename (see ``resources/chardicts`` directory)
                phono_char_name (str): the phonological characters filename (see ``resources/chardicts`` directory)
                enable_phono (bool): if True, it enables the phonological subsystem
                store_ortho (bool): if True, it stores output orthographic probability distributions (percepts, words and lexical decision)
                store_phono (bool): if True, it stores output phonological probability distributions (percepts, words and lexical decision)
                top_down_influence (bool): if True, it allows lexical representation feedback on orthographic representations
                top_down_influence_phono (bool): if True, it allows phonological representation feedback on lexical representations
                version (int): frequency table use and Lexical decision computing method. Version 0 refers to the plain vanilla version, version 7 refers to L2 normalization whereas version 9 refers to L2 normalisation + Log frequency. Defaults to 0.

            Returns:
                self (:obj:`braid`).
        """
        self.langue = langue
        self.lexicon_name = lexicon_name
        self.lexicon = None                # initialized at None, will be specified later
        self.freq = None                    # initialized at None, will be specified later

        # ortho/phono information
        self.dist_names = ["percept", "word", "ld", "TDI"]
        self.shared_parameters = ['Qa', 'QPhon', 'sdA', 'sdM', 'gazePosition', 'meanA', 'pos', 'top_down_influence', 'leakP', 'leakPhon', 'leakW',
                                  'crowding', 'scaleI', 'slopeG', 'version', 'confFactor', 'att_factor']

        self.ortho = mod.orthography(**{"model": self, "stim": stim, "mod": "ortho", "eps": 0.001, "learning": False, "dist": {key: None for key in self.dist_names},
                                        "top_down_influence": top_down_influence, "enabled": True, "store": store_ortho, "remove_stim": remove_stim_ortho,
                                        "chars_filename": ortho_char_name, "conf_mat_name": conf_mat_name})
        self.phono = mod.phonology(**{"model": self, "mod": "phono", "eps": 0.001, "learning": False, "dist": {key: None for key in self.dist_names + ["phono_tmp"]},
                                      "top_down_influence": top_down_influence_phono, "enabled": enable_phono, "store": store_phono, "remove_stim": remove_stim_phono,
                                      "chars_filename": phono_char_name})

        # Model parameter values
        self.meanA = 0
        self.gazePosition = 0
        self.sdA = 1.75
        self.sdM = 5
        self.leakW = 1250
        self.weightLDYES = self.weightLDNO = 0.15
        self.ld_trans_mat = np.array(
            [[1 - self.weightLDYES, self.weightLDYES], [self.weightLDNO, 1 - self.weightLDNO]])

        # Each version corresponds to a specific way of computing lexical
        # decision (ld) :: 0=Ali, 7=L2, 9=L2+Log
        self.version = version

        self.reset = {"dist": True, "lexicon": False}

        # Initializing BRAID model
        self.start()

    @property
    def gazePosition(self):
        u""" Gaze position attribute getter.

        Returns:
            (float): attribute value
        """
        return self._gazePosition

    @gazePosition.setter
    def gazePosition(self, value):
        u""" Gaze position attribute setter.

            Args:
                value (float): attribute value
        """
        if value < -1 or value >= self.ortho.N_max:
            logging.warning(
                "Out of range position value. Gaze Position is usually defined within the stimulus limits (0<=g<=N).")
        self._gazePosition = value

    @property
    def meanA(self):
        u""" Visual attention focus attribute getter.

        Returns:
            (float): attribute value
        """
        return self._meanA

    @meanA.setter
    def meanA(self, value):
        u"""Visual attention focus attribute setter.

        Args:
            value (float): attribute value
        """
        if value < -1 or value >= self.ortho.N_max:
            logging.warning(
                "Out of range position value. Attention focus position should be defined within the stimulus limits (0<=mu<=N).")
        self._meanA = value

    @property
    def pos(self):
        u"""Visual attention focus attribute getter.

        Returns:
            (float): attribute value
        """
        return self._meanA

    @pos.setter
    def pos(self, value):
        u""" Both gaze position and visual attention focus attribute setter.

        Args:
            value (float): attribute value
        """
        if value < -1 or value >= self.ortho.N_max:
            logging.warning(
                "Out of range position value. Position value should be defined within the stimulus limits.")
        self.meanA = value
        self.gazePosition = value
        self.init_bottom_up_process()

    @property
    def sdM(self):
        u"""Maximal value for visual attention dispersion attribute getter.

        Returns:
            (float): attribute value
        """
        return self._sdM

    @sdM.setter
    def sdM(self, value):
        u"""Maximal value for visual attention dispersion attribute setter.

        Args:
            value (float): attribute value
        """
        if value < 0:
            logging.warning("Negative value of standard deviation.")
        self._sdM = value
        self.__setattr__('sdA', self.sdA)

    @property
    def sdA(self):
        u"""Visual attention dispersion attribute getter.

        Returns:
            (float): attribute value
        """
        return self._sdA

    @sdA.setter
    def sdA(self, value):
        u"""Visual attention focus attribute setter.

        Args:
            value (float): attribute value
        """
        if value < 0:
            logging.warning("Negative value of standard deviation.")
        sdM = self.__getattribute__("sdM") if hasattr(self, 'sdM') else 10000
        self._sdA = min(value, sdM)

    @property
    def stim(self):
        u"""Stimulus attribute getter.

        Returns:
            value (str): attribute value
        """
        return self.ortho.stim

    @stim.setter
    def stim(self, value):
        u"""Stimulus attribute setter.

        Args:
            value (str): stimulus character string
        """
        # sets stim and affects the new value for the current N
        self.ortho.__setattr__('stim', value)

    def __setattr__(self, name, value):
        u"""Universal attribute setter.
        This method checks first if the attribute exists in the ortho/phono attributes (:obj:`modality`).
        If it isn't the case, it looks for it in the superclass (:obj:`braid` class).

        Args:
            name (str): attribute name
            value: attribute value
        """
        # first, check if the attribute exists in this class. Then look for it
        # in the super class
        if name in ['Qa', 'eps', 'top_down_influence',
                    'leakP', 'crowding', 'scaleI', 'slopeG']:
            self.ortho.__setattr__(name, value)
        elif name in ['QPhon', 'leakPhon']:
            self.phono.__setattr__(name, value)
        else:
            super().__setattr__(name, value)

    ################## INIT METHODS #################################

    ##### LOAD DATA ######
    def extract_proba(self):
        u"""Extract the word prior probability :math:`P(W^0)` for the lexical submodel.
        """
        if self.version == 9:
            f_type = "freq_log"
            self.lexicon["freq_log"] = np.log(self.lexicon.freq + 1)
        else:
            f_type = "freq"

        self.lexicon["p"] = self.lexicon[f_type] / self.lexicon.merge(self.lexicon.groupby(
            'len').sum(), left_on='len', right_index=True)[f_type + '_y']
        self.freq = np.array(self.lexicon[self.lexicon.len == self.ortho.N].p)

    def set_lexicon(self):
        u"""Implements preprocessing of the lexicon dataframe.

            - word length is matched between words by adding ``#`` (padding);
            - lexicon is grouped by orthographic length `len`;
            - word frequencies are extracted and lexical knowledge is built.
        """

        if 'pron' in self.lexicon.columns:
            ml = self.lexicon.groupby('len').agg(
                {'phlen': 'max'}).rename({"phlen": "max_phlen"}, axis=1)
            lx = self.lexicon.reset_index().merge(ml, on="len")
            lx["dz"] = "#"
            lx["pron"] = lx["pron"].str.replace(
                '#', '') + lx.dz * (lx.max_phlen - lx.phlen)
            self.lexicon = self.lexicon.drop("pron", axis=1).merge(
                lx[['word', 'pron']], on='word').set_index('word')

        self.lexicon["idx"] = self.lexicon.groupby("len").cumcount()
        self.extract_proba()
        self.build_top_down()

    def extract_lexicon(self):
        u"""Loads lexicon and keep the orthographical and/or phonological informations according to ``ortho.store`` and ``phono.store`` values.

            - if ``phono.store == False``, no phono information is stored
            - if ``ortho.store = False``, stimulus name is stored (key in all dicts, but boolean ortho is created)

        This only called by ``start()`` and ``add_lexicon()`` functions.

            Returns:
                lexicon (pandas.DataFrame)
        """

        return lxc.extractLexicon(lexicon_name=self.lexicon_name, fMin=0, fMax=sys.maxsize, return_df=True,
                                  forbid_list=None, ortho=self.ortho.store, phono=self.phono.store)

    def add_lexicon(self, lexicon_name, store_ortho=None,
                    store_phono=None, nmax=None):
        """Adds words from a new lexicon to the current one. (not used for the first one)
        Useful to store words known only written/spoken and other known in both modalities.
        Choose the modality to store with ``store_ortho`` and ``store_phono``.
        """
        self.lexicon_name = lexicon_name
        self.ortho.store = store_ortho if store_ortho is not None else self.ortho.store
        self.phono.store = store_phono if store_phono is not None else self.phono.store
        lex = self.extract_lexicon()
        # trie le nouveau lexique par longueur de mots, en gardant maximum nmax
        # mots par longueur
        if nmax is not None:
            lex = lex.groupby('len').apply(lambda x: x.head(
                nmax) if x.shape[0] >= nmax else x).droplevel(0)
        lex = lex.dropna(how='all')
        if self.lexicon is not None:  # already a lexicon before the add -> must merge the 2 lexicon
            self.lexicon = self.lexicon.merge(
                lex, on='word', how='outer')  # .fillna(method='ffill')
            self.lexicon.fillna({key: False for key in [
                                'ortho_x', 'ortho_y', 'phono_x', 'phono_y']}, inplace=True)
            self.lexicon['len'] = self.lexicon[['len_x', 'len_y']].max(axis=1)
            self.lexicon['freq'] = self.lexicon[[
                'freq_x', 'freq_y']].max(axis=1)  # TODO
            self.lexicon['ortho'] = self.lexicon['ortho_x'] | self.lexicon['ortho_y']
            self.lexicon['phono'] = self.lexicon['phono_x'] | self.lexicon['phono_y']
            l = ['ortho', 'freq', 'len']
            if 'phlen_x' in self.lexicon.columns:
                self.lexicon['phlen'] = self.lexicon['phlen_y'] if self.phono.store else self.lexicon['phlen_x']
                self.lexicon['pron'] = self.lexicon['pron_y'] if self.phono.store else self.lexicon['pron_x']
                l += ['phlen', 'pron', 'phono']
            self.lexicon.drop([c for c in self.lexicon.columns if c[:-2] in l],
                              inplace=True, axis=1, errors='ignore')
        else:
            self.lexicon = lex
        self.set_lexicon()

    def start(self):
        u""" Starting procedure of the BRAID model.

            - loads lexicon (`self.lexicon_name`): requires at least word, freq and len columns (if ``self.langue`` is specified, the lexicon is automatically chosen)
            - loads confusion matrix (`self.ortho.conf_mat name`)
            - loads orthographic and phonological char (`self.ortho.chars_filename` and `self.phono.chars_filename`)
            - all these files must be in the `resources` directory.
        """

        if len(self.lexicon_name) == 0:
            if self.langue == "en":
                self.lexicon_name = "celex.csv" if self.phono.enabled else "ELP.csv"
            elif self.langue == "ge":
                self.lexicon_name = "celex_german.csv"
            elif self.langue == "fr":
                self.lexicon_name = "lexique3_red.csv"

        self.ortho.set_char_dict()
        self.phono.set_char_dict()
        self.ortho.set_confusion_matrix()
        self.lexicon = self.extract_lexicon()
        self.set_lexicon()
        logging.braid(f"""\t Lexicon loaded! size: {self.lexicon.size}, \t name: {self.lexicon_name}
         Confusion matrix loaded! size: {self.ortho.conf_mat.size} \t name: {self.ortho.conf_mat_name}
         Alphabet character dictionnary is loaded : {self.ortho.chars_filename}
         Phonetic character dictionnary is loaded : {self.phono.chars_filename} """)

    ############# INIT BOTTOM UP ##############

    def init_bottom_up_process(self):
        """Initialize low-level vision and attentional mechanisms.

            - confusion matrix (letter similarity)
            - crowding
            - visual acuity gradient
            - visual attention distribution
            - phonological attention distribution (only if ``phono.enabled == True``)
        """
        self.ortho.build_bottom_up_ortho()
        if self.phono.enabled:
            self.phono.build_attention_distribution()

    ############ INIT TOP DOWN #################

    def build_top_down(self):
        u"""Extract lexical knwoledge from lexicon.
        To speed up computations, this method is only called once at Braid init procedure.
        """
        # Build Orthographic and Phonological knowledge
        self.ortho.build_whole_knowledge()
        if self.phono.enabled:
            self.phono.build_whole_knowledge()

    def set_top_down(self):
        u"""Sets lexical knowledge information according to the chosen stimulus length.
        First, it sets orthographic and phonological lexical knowledge, then it extracts lexical prior probabilities."""
        if self.ortho.enabled and self.ortho.exists:
            self.ortho.set_knowledge()
        if self.phono.enabled and self.phono.exists:
            self.phono.set_knowledge()
        if self.ortho.exists or self.phono.exists:
            self.extract_proba()

    ################# MODEL CONFIGURATION FOR SIMU #########################

    def reset_model(self):
        """Resets `top down` (lexical) and `bottom up` (perceptual) information.
        If needed, it resets lexicon, positions and probability distributions."""
        if self.reset["lexicon"]:
            self.build_top_down()
        self.set_top_down()  # needed for reset_dist(word)
        if self.reset["dist"]:
            self.ortho.reset_dist()
            self.phono.reset_dist()
        self.init_bottom_up_process()  # needs position to be set before

    ########### MAIN #########################

    def one_step_normal(self):
        """Executes one iteration of simulation."""
        # Orthographic processing
        self.ortho.build_percept()
        self.ortho.one_step_modality()
        self.ortho.update_percept()

        # Phonological processing
        self.phono.one_step_modality()
        if self.phono.exists:
            self.phono.update_percept()
