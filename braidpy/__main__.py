# -*- coding: utf-8 -*-
"""
__main__.py, a file of the BRAID software
Copyright 2021, CNRS, UGA
Authors: Diard, Valdois, Phénix, Ginestet, Saghiran, Steinhilber
Contact: Julien Diard, julien.diard@univ-grenoble-alpes.fr
Licence: CECILL-2.1
Creation date: -
"""

import argparse
import logging
import time
import timeit
from copy import copy

from braidpy.simu import simu
from braidpy.GUI.gui import gui
import pdb
import os
import pickle as pkl
from tornado.ioloop import IOLoop
from bokeh.server.server import Server
from bokeh.application import Application
from bokeh.application.handlers.function import FunctionHandler
import numpy as np


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', default=1, type=int, help='Number of simulations')
    parser.add_argument('-s', default=None, type=str, help='String to identify')
    parser.add_argument('--phono_input', default=None, type=str, help='phono input : in ortho (ex any) or phono (ex /EnI/) form' )
    parser.add_argument('-m', default=None, type=int, help='Max number of iterations')
    parser.add_argument('--lex', default=None, type=str, help='Lexicon name')
    parser.add_argument('-l', default=None, type=str, help='language : en or fr')
    parser.add_argument('--lexicon', default=None, type=str, help='lexicon name')
    parser.add_argument('-f', default=None, type=str, help='fixed frequency of the stimulus')
    parser.add_argument('-t', default=None, type=str, help='Type of simulation')
    parser.add_argument('-u', default=None, type=bool, help='Allows the model to update knowledge')
    parser.add_argument('-v', default=None, type=int, help='Version of the lexical decision')
    parser.add_argument('-p', default=None, type=int, help='position of eye/attention')
    parser.add_argument('-g', action='store_true', help = 'graphic interface')
    parser.add_argument('--Qa', default=None, type=float, help='Value of Qa parameter')
    parser.add_argument('--sdA', default=None, type=float, help='Value of sdA parameter')
    parser.add_argument('--sdM', default=None, type=float, help='Value of sdM parameter')
    parser.add_argument('--thrExpo', default=None, type=float, help='exposition threshold during visual exploration')
    parser.add_argument('--stop', default=None, type=str, help='stop criterion for the simulation')
    parser.add_argument('--thrFix', default=None, type=float, help='fixation threshold during visual exploration')
    parser.add_argument('--alpha', default=None, type=float, help='motor cost during visual exploration')
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--forceApp', action='store_true')
    parser.add_argument('--forceUpdate', action='store_true')
    parser.add_argument('--forceWord', action='store_true')
    parser.add_argument('--NoTD', action='store_true')
    parser.add_argument('--phono', action='store_true')
    parser.add_argument('--level', default=None, type=str, help='log level (info, debug)')
    parser.add_argument('--time', action='store_true', help = 'measures time to execute')
    args = parser.parse_args()
    dico={'s':'stim','m':'max_iter','lex':'lexicon_name','t':'simuType','u':'learning','v':'version',
          'phono':'enable_phono','level':'level','l':'langue','phono_input':'phono_input', 'stop':'stopCriterionType'}
    param={value : getattr(args,key) for key,value in dico.items() if getattr(args,key) is not None}
    param['path']='/home/alexandra/braidpy/'
    sim=simu(**param)
    for param in ['Qa','sdA','sdM','thrExpo','thrFix','alpha', 'forceApp','forceUpdate','forceWord']:
        val= getattr(args,param)
        if val is not None:
            setattr(sim,param,val)
    if args.NoTD:
        sim.model.top_down_influence=False
    if args.p is not None:
        sim.model.pos=args.p
    if args.f is not None:
        sim.model.change_freq(args.f,sim.stim)
    if args.g:
        def make_document(doc):
            GUI = gui(sim)
            for att in dir(doc):
                try:
                    setattr(doc,att,getattr(GUI.curdoc,att))
                except:
                    pass
                    # print("att",att)
        io_loop = IOLoop.current()
        server = Server(applications={'/': Application(FunctionHandler(make_document))}, io_loop=io_loop, port=5001)
        server.start()
        server.show('/')
        io_loop.start()
    else:
        if args.time:
            t=time.time()
            #t = timeit.Timer(sim.run_simu_H)
            #t.timeit(2)
        for i in range(args.n):
            sim.run_simu_general()
        if args.time:
            print("t1",time.time()-t)
        #timeit.timeit('sim=simu(); sim.run_simu_general()', number=10)
    if args.debug :
        pdb.set_trace()
