#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
simu.py: [a file of the BRAID software]"""
__copyright__ = "2021, CNRS, UGA"
__authors__ = "Diard, Valdois, Phénix, Ginestet, Saghiran, Steinhilber"
__contact__ = "Julien Diard, julien.diard@univ-grenoble-alpes.fr"
__licence__ = "CECILL-2.1"
__date__ = "Aug 23 2021"

# Scientific/Numerical computing
from scipy.stats import entropy
import pickle as pkl
import numpy as np
from collections import defaultdict

# General purpose libraries
import os, logging

# BRAID utlities
from braidpy.braid import braid
import braidpy.utilities as utl

# logging levels
try:
    utl.addLoggingLevel("BRAID", 11)
    utl.addLoggingLevel("SIMU", 12)
    utl.addLoggingLevel("GUI", 13)
    utl.addLoggingLevel("EXPE", 21)
except AttributeError:
    logging.warning(
        "Some (or All) Logging levels are already defined in logging module.")
    pass


def generic_simu(func):
    u"""Simulation decorators (wrapper): decorators/wrappers allow to run parts of code before/after each decorated function.

    ``@generic_simu`` implements the common beginning ang ending procedures need for all simulation.

    Note:
        If you are unfamiliar whith these concepts and, to get more information about how decorators and function wrapper works in python,
        please take a look on some available resources about these features:

            - https://www.python.org/dev/peps/pep-0318/
            - https://realpython.com/primer-on-python-decorators/
    """

    def wrapper(self, *args, **kwargs):
        spec = utl.fullspec(func, *args, **kwargs)
        if spec['sdA'] is not None:
            self.begin_simu(
                stim=spec['stim'],
                pos=spec['pos'],
                sdA=spec['sdA'])
        else:
            self.begin_simu(stim=spec['stim'], pos=spec['pos'])
        res = func(self, *args, **kwargs)
        self.end_simu()  # formater les distributions dans un format facilement utilisable
    return wrapper


class simu:
    u"""Simulation context, includes model, simulation parameters and the outputs."""

    def __init__(self, max_iter=1000, stim="help", model=None, use_pkl=False, save_pkl=False, build_prototype=False,
                 alpha=0.1, one_step_type="normal", level='simu', **model_parameters):
        u"""Object constructor, simulation context initiator

            Args:
                max_iter (int): number of iterations
                stim (str): char string to read/recognize
                model_parameters: Arguments values given directly to the model instanced whithing this simulation context:

                    - **langue** (*str*): lexicon language "en", "fr" or "ge" (/!\ if langue is specified, do not specify lexicon_name and *vice versa*)
                    - **lexicon_name** (*str*): lexicon filename (see `resources/lexicon` directory)
                    - **conf_mat_name** (*str*): the letter confusion matrix filename (see `resources/confusionMatrix` directory)
                    - **ortho_char_name** (*str*): the orthographic characters filename (see `resources/chardicts` directory)
                    - **phono_char_name** (*str*): the phonological characters filename (see `resources/chardicts` directory)
                    - **enable_phono** (*bool*): when it is set on True, phonological level computations are performed (as described in BRAID-Phon model). Otherwise, computations are limited to orthographic aspects (like in BRAID model)
                    - **store_phono** (*bool*): store the phonology in the lexicon
                    - **store_ortho** (*bool*): store the orthography in the lexicon

                model (:obj:`braid`): braid class instance. Default value is `None` which implies instancing a new model
                use_pkl (bool): if True, uses a precalculated model
                save_pkl (bool): if True, stores the calculated model
                build_prototype (bool): if True, build the prototype from the simulation file
                alpha (float): eye movement motor cost [EXPERIMENTAL]
                one_step_type (str): Defaults to normal
                level (str): Defaults to simu

            Returns:
                self (:obj:`simu`).
        """

        if model_parameters is None:
            model_parameters = {}

        self.model = braid(**model_parameters) if model is None else model
        self.level = level
        self.max_iter = max_iter

        # values to store
        self.store = {key: True for key in self.dist_names}

        # simulation result datasructure
        self.res = {
            mod: {
                key: defaultdict(list) for key in self.dist_names} for mod in [
                "ortho",
                "phono"]}

        # successive fixations
        self._fix = {"att": [], "t": [], "pos": [], "sd": []}
        self.t_tot = 0
        self.alpha = alpha
        if save_pkl:
            self.save_model()
        if build_prototype:
            self.build_prototype()
        self.stim = stim
        self.one_step_type = one_step_type

    # Model & parameters handling methods (before launching simulations)

    def __setattr__(self, name, value):
        u"""General (universal) attribute value setter.

        Args:
            name (str): Attribute name.
            value: Attribute value.
        """
        if 'model' in self.__dict__ and name in self.model.shared_parameters:
            self.model.__setattr__(name, value)
        else:
            super().__setattr__(name, value)

    def __getattr__(self, name):
        u"""General (universal) attribute value getter.

        Args:
            name (str): Attribute name.

        Returns:
            Attribute value.
        """
        # look for the attribute in model if it doesnt exist in simu
        if 'model' in self.__dict__:  # and name in self.model.__dict__:
            return self.model.__getattribute__(name)

    def set_model(self, model):
        """Set model attribute.

        Args:
            model (:obj:`braid`):
        """
        self.model = model

    @property
    def stim(self):
        u"""Stimulus attribute (`self.stim`) getter."""
        return self.model.stim

    @stim.setter
    def stim(self, value):
        u"""Stimulus attribute (`self.stim`) setter.
        This setter sets stim and affects the new value for the current `N` attribute (stimulus length)."""
        self.model.__setattr__("stim", value)

    @property
    def level(self):
        u"""Stimulus attribute (`self.level`) getter."""
        return self._level

    @level.setter
    def level(self, value):
        u"""Stimulus attribute (`self.level`) setter."""
        self._level = value
        try:
            logging.getLogger().setLevel(getattr(logging, value.upper()))
        except BaseException:
            print("except")
            logging.basicConfig(
                level=getattr(
                    logging,
                    value.upper()),
                format='%(levelname)s - %(message)s')

    def save_model(self):
        u"""Saves the model to save calculation next time. One can load one model for each lexicon and confusion matrix.
        Please check if the folder `models` exists already. If it's not, please create this directory before calling this method."""
        filename = self.path + "braidpy/models/" + \
            self.model.lexicon_name[:-4] + "--" + \
            self.model.conf_mat_name[:-4] + ".pkl"
        with open(filename, "wb") as f:
            pkl.dump(self.model, f)

    def import_model(self, langue, lexicon_name, conf_mat_name):
        """ if a model corresponding to the lexicon and the confusion matrix is saved,
            load the model instead of calculate it """

        lexicon_name = lexicon_name if lexicon_name else "celex.csv" if langue == "en" \
            else "lexiconFLP_Basic_ph.csv"
        conf_mat_name = conf_mat_name if conf_mat_name else "TownsendMod.xls"
        filename = self.path + "braidpy/models/" + \
            lexicon_name[:-4] + "--" + conf_mat_name[:-4] + '.pkl'
        if os.path.exists(filename):
            with open(filename, 'rb') as f:
                return pkl.load(f)
        return None

    # Adjunct methods

    def complete_dist(self):
        u"""Completes the simu results with current model distributions.
        All probability distributions resulting from the simulation are stored in numpy ndarrays
        when they are set to True in the attribute dictionnary ``self.store``."""
        for mod in ["phono", "ortho"]:
            for name in self.dist_names:
                self.res[mod][name][self.stim] += [
                    getattr(self.model, mod).dist[name]] if self.store[name] else []

    def begin_simu(self, stim="", pos=0, sdA=None):
        u"""Simulation beginning procedure.

            - Prints stimulus character string
            - Deletes older simulation data with the same stimulus (Please save older data before launching a new simualtion with the same stimulus)
            - Sets Gaze and VA parameters. Default ``pos`` is 0 and default ``sdA`` is 1.75
            - Extract lexical prior probabilities
            - Initialize eye position and fixation durations datastructure (useful when considering moving eye and attention simulations)
        """
        self.stim = stim if stim else self.stim
        logging.simu(f"Stimulus: {self.stim}")
        # delete all previous simulations with this stim
        self.remove_stim_res()

        # handle attention distribution (TODO : Make it simple)
        if sdA is not None:
            self.model.sdA = sdA
        if pos != -1:
            self.model.pos = pos

        self.model.reset_model()
        if self.model.ortho.remove_stim and self.model.phono.remove_stim:
            self.model.change_freq(0)
            self.model.extract_proba()
        self.t_tot = 0
        self.fix = {"t": [], "att": [], "pos": [], "sd": []}
        self.update_fix_info()
        self.complete_dist()

    def update_fix_info(self):
        u"""Creates a new fixation data.
        Basically, it adds current gaze and VA parameters to eye position and fixation durations datastructure."""
        self.fix["pos"].append(self.model.meanA)
        self.fix["sd"].append(self.model.sdA)
        self.fix["t"].append(self.t_tot)
        self.fix["att"].append(self.model.ortho.attention_profile)

    def delete_fixation(self, t):
        u"""[EXPERIMENTAL] Removes data specific to one fixation."""
        for mod in ["phono", "ortho"]:
            for name in self.dist_names:
                self.res[mod][name][self.stim] = self.res[mod][name][self.stim][:-
                                                                                max(t, 1)]
                # The model must be reset to its initial state (before fixation
                # suppression)
                getattr(self.model,
                        mod).dist[name] = self.res[mod][name][self.stim][-1]
        for k, val in self.fix.items():
            self.fix[k] = val[:-1]
        self.t_tot -= t
        logging.simu(f"fixation removed: {t} iterations")

    def end_simu(self):
        u"""Simulation ending procedure.
        In the end of a simulation, this method changes the shape of the results to use them easily (inverting last dimension with the first one of the result numpy.ndarray).
        The resulting arrays have iterations in the last dimension. For phonemes and letters, the first dimension corresponds to positions and teh second one to letters."""

        for mod in ["ortho", "phono"]:
            for name in self.dist_names:
                self.res[mod][name][self.stim] = np.moveaxis(
                    self.res[mod][name][self.stim], 0, -1)

        if self.phono.enabled and self.phono.exists:
            self.generate_pron()
        if self.model.ortho.remove_stim and self.model.phono.remove_stim:
            self.model.change_freq(self.model.old_freq)
            self.model.extract_proba()

        logging.simu(f"simulation duration: {self.t_tot}")
        self.ortho.print_all_dists()
        self.phono.print_all_dists()
        logging.simu(f"fixation position(s): {self.fix['pos']}")
        logging.simu(f"fixation VA dispersion(s): {self.fix['sd']}")

    def generate_pron(self):
        u"""Generates pronounciation based on integral of phonological probability evolution curves.
        The winning pronunciation in each phonological position is the one with the highest probability
        after taking into account the simulation total number of iterations.

        Returns:
            pron (str): character string of the pronouncitaion (using the phoneme coding, see phon_char file).
        """
        phono_output = self.get_res(mod="phono", dist="percept")
        chars = self.model.phono.chars
        self.model.phono.pron_idx = phono_output.sum(2).argmax(axis=1)
        self.model.phono.pron = "".join(
            [chars[i] for i in self.model.phono.pron_idx])
        return self.model.phono.pron

    # Result handling

    def remove_stim_res(self):
        u"""Removes all resulting distributions corresponding to a stimulus (``self.stim``)
        from the result dictionary."""
        for mod in ["ortho", "phono"]:
            for name in self.dist_names:
                self.res[mod][name][self.stim] = []

    def get_res(self, dist="ld", mod="ortho", stim=None):
        """Returns the "desired" output probability distribution.
        Calling this method is equivalent to executing : ``self.res[mod][dist][stim]``

        Args:
            dist (str): ld, percept or word. Defaults to ld.
            mod (str): ortho or phono. Defaults to ortho.
            stim (str):  stimulus. Defaults to ``None`` (equivalent to self.stim).

        Returns:
            (np.ndarray): Probability distribution values.

        Some examples:
            >>> self.get_res(dist="percept", mod="ortho")   # gets the letter identity distributions
            >>> self.get_res(dist="percept", mod="phono")   # gets the phoneme identity distributions
            >>> self.get_res(dist="word", mod="ortho")      # gets the word identity distribution
            >>> self.get_res(dist="ld", mod="ortho")        # gets (the orthographic) lexical decision
        """
        string = stim if stim is not None else self.stim
        return self.res[mod][dist][string]

    # One iteration of simulation

    def run_simu_general(self, stim="", pos=-1, sdA=None):
        u"""Runs a simulation given considered simulation (`one_step_type`).
        Only the type "normal" is considered here."""
        # other possible values : normal, app, change_pos, grid_search, H. ==>
        # NOT available in this version
        getattr(simu, "run_simu_" + self.one_step_type)(self, stim, pos)

    def one_step_general(self):
        u"""Runs one step of a simulation.
        Simulation type (`one_step_type`) is taken into consideration."""
        getattr(self.model, "one_step_" + self.one_step_type)()
        self.complete_dist()
        self.t_tot += 1

    # Simulation launching methods

    @generic_simu
    def run_simu_normal(self, stim="", pos=-1, sdA=None):
        u"""Runs the default simulation for `max_iter` iterations.
        Note Args exists only for compatibility with unpublished versions of the code.
        These have no effect here.
        Args:
            stim (str, optional): Defaults to "".
            pos (int, optional): Defaults to -1.
            sdA ([type], optional): Defaults to None.
        """
        for t in range(self.max_iter):
            self.one_step_general()
