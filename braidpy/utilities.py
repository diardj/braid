#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""utilities.py: [a file of the BRAID software] This file contains independent helper functions that implement recurring computations when running the BRAID model."""
__copyright__ = "2021, CNRS, UGA"
__authors__ = "Diard, Valdois, Phénix, Ginestet, Saghiran, Steinhilber"
__contact__ = "Julien Diard, julien.diard@univ-grenoble-alpes.fr"
__licence__ = "CECILL-2.1"
__date__ = "Aug 23 2021"

# General purpose libraries
import sys, inspect, logging

# Scientific computation libraries
import pandas as pd
from scipy.stats import norm
from numpy import linalg as LA
import numba
import numpy as np



# Visual low-level processing helper funcs
def build_interference_weight(crowding, length):
    u"""Computing the interference weights matrix. This helps to compute the probability distribution :math:`P(I~|~S)`.

    Args:
        crowding (float): crowding parameter value :math:`\\theta`.
        length (int): stimulus length :math:`N`.

    Returns:
        mat (numpy.ndarray): square matrix with :math:`\\theta` value on the diagonal and :math:`1-\\frac{\\theta}{2}` on the top and bottom diagonal values.
    """
    mat = np.zeros((length, length))
    for i in range(0, length):
        for j in range(0, length):
            if i == j:
                mat[i, j] = crowding
            elif i in (j - 1, j + 1):
                mat[i, j] = (1 - crowding) / 2
    mat = mat / mat.sum(axis=1)[:, np.newaxis]
    return mat


# Lexical processing helper funcs
@numba.jit(nopython=True, debug=False)
def build_word_transition_vector(word_proba, wfreq, word_leak):
    u"""Implementing the lexical short-term memory. This helps to compute the probability distribution :math:`P(W^t~|~W^{t-1})` at each iteration :math:`t`.

    Args:
        word_proba (numpy.ndarray): the previous word identity probability distribution.
        wfreq (numpy.ndarray): the prior on word probabilities (reference frequencies) :math:`P(W^0)`.
        word_leak (float): corresponds to the parameter :math:`\\textit{Leak}_W`.

    Returns:
        res (numpy.ndarray): the current word identity probability distribution.
    """
    lng = word_proba.shape[0]
    res = np.zeros(lng)
    for i in range(lng):
        res[i] = (word_proba[i] + wfreq[i] * word_leak)
    return res / (1 + word_leak)


@numba.jit(nopython=True, debug=False)
def create_repr(words_indices, size, eps):
    """ This function build the lexical (orthographic) knowledge of selected list of words.

    Args:
        words_indices (numpy.ndarray): a two-dimensional array with words on the 1\\ :sup:`st` dimension and letter (or phoneme) position
            on the 2\\ :sup:`nd`. Letter (or phoneme) identity is represented using letter (or phoneme) index (see orth_char and phon_char files).
        size (int):  orthographic (or phonological) space size :math:`\\mathcal{D}_L`.
        eps (float): epsilon value (:math:`\\epsilon_L` or :math:`\\epsilon_\\phi`).

    Returns:
        arr (numpy.ndarray): a three-dimensional array of lexical knowledge that corresponds to the
        probability distribution of letter or phoneme identity given words (:math:`P(L~|~W)` et :math:`P(\\phi~|~W)`).
        In this array, the 1\\ :sup:`st` dimension corresponds to words, the 2\\ :sup:`nd` corresponds to positions and the 3\\ :sup:`rd` to
        letter or phoneme indices.
    """
    wdi = np.shape(words_indices)
    arr = np.zeros((wdi[0], wdi[1], size), dtype=np.float32)
    # ici rajouter fonction fill numba
    for iwd in range(arr.shape[0]):
        wd = words_indices[iwd]
        for iN in range(arr.shape[1]):
            letter_index = wd[iN]
            for iL in range(arr.shape[2]):
                # genrate a pseudodirac probability distribution
                arr[iwd, iN, iL] = 1 - eps * \
                    (size - 1) if iL == letter_index else eps
    return arr


@numba.jit(nopython=True, debug=False)
def wsim(repr, percept):
    u"""An alternative implementation of (np.einsum('jk,ijk->ij', percept, repr)`` with additional computations relative to lexical decision.
    This corresponds to the intermediate Bayesian inference allowing to merge lexical knowledge and knowledge about letters identity
    in order to compute both word probabilty distribution and error probability in words spelling.

    Args:
        repr (numpy.ndarray): the three dimensional array representing whole lexical knowledge
            with words in the 1\\ :sup:`st` dimension, orthographic positions in the 2\\ :sup:`nd` and letters in the 3\\ :sup:`rd` dimension.
        percept (numpy.ndarray): the current array reprensenting probabilities of letter identity :math:`P(P^t~|~K^t)`.

    Returns:
        res (numpy.ndarray): a two-dimensional array with words in the 1\\ :sup:`st` dimension and on the 2\\ :sup:`nd` dimension, the first "column"
        corresponds to ``np.einsum('jk,ijk->ij', percept, repr)``, the other columns corresponds to the cases where the stimulus is supposed "incorrect".
        Each one of these case suppose that the stimulus contains one error (one letter that does not match) in a specific orthographic position.

    """
    # For each word "m", we want to compute the product of  pok*m * perr*m when an error is supposed,
    # This is equivalent to : product(pok*m) * perr_j/pok_j *m
    sh = repr.shape
    # lexicon words, ok+ as many errors as letters
    res = np.ones((sh[0], sh[1] + 1))
    for i in range(
            sh[0]):                                  # for all lexicon words
        for j in range(sh[1]):  # position des erreurs
            tmp_err = 0
            tmp_ok = 0
            for k in range(sh[2]):
                m = repr[i, j, k]
                p = percept[j, k]
                tmp_err += (1 - p) * m
                tmp_ok += p * m
            # error in the j position
            res[i, j + 1] *= tmp_err / tmp_ok if tmp_ok > 0 else 0
            # in the end,  we get the product the of ok case
            res[i, 0] *= tmp_ok
        for j in range(sh[1]):                              # Error positions
            res[i, j + 1] *= res[i, 0]
    return res


@numba.jit(nopython=True, debug=False)
def wsim_mask(repr, percept, mask):
    """An alternative implementation of ``np.einsum('jk,ijk->ij', percept, repr)`` with additional computations relative to error pattern mask application.

    Args:
        repr (numpy.ndarray): the three dimensional array representing whole lexical knowledge with words in the 1\\ :sup:`st` dimension, orthographic positions in the 2\\ :sup:`nd` and letters in the 3\\ :sup:`rd` dimension.
        percept (numpy.ndarray): the current array reprensenting probabilities of letter identity (:math:`P(P^t~|~K^t)`).
        mask (list): a list (of numpy arrays) of bool representing the supposed error pattern.

    Returns:
        (list): a list of two numpy.ndarray such as the first corresponds to ``np.einsum('jk,ijk->ij', percept, repr)`` and the second
        is a 2-dimentional array that corresponds to word probabilities (1\\ :sup:`st` dimension) given each mask pattern.
    """
    sh = repr.shape
    res = np.ones((sh[0], sh[1]))
    res_mask = np.ones((sh[0], sh[1]))
    for l in range(sh[0]):                          # words from lexicon
        for i in range(sh[1]):                      # error positions
            tmp = 0
            for j in range(sh[2]):
                tmp += repr[l, i, j] * percept[i, j]
            res[l, i] *= tmp
            res_mask[l, i] *= tmp if mask[i] == 0 else 1
    return [res, res_mask]


# Probability distributions

def uniform(length):
    u"""Generates a discrete Uniform distribution.

    .. math::
        P([X = x]) = \\frac{1}{\\mathcal{D}} , \\forall x

    Args:
        length (int): Space size :math:`\\mathcal{D}`.

    Returns:
        vect (numpy.ndarray): 1D numpy array of distribution probability values.
    """
    return np.ones(length) / length


def pseudodirac(maximum, length, eps):
    u"""Generates a PseudoDirac (discrete) distribution. A very high probability is assigned to a
    given value (of the probabilitic variable) and a very low probability in the other values.
    Mathematically, if :math:`P(X)` is a pseudo-Dirac distribution, we note:

    .. math::
        P([X = x]) = \\left\\{
        \\begin{array}{l}
            1 - (\\mathcal{D} - 1)~\\epsilon, \\text{  if } x = x_{\\textit{max}}\\
            \\epsilon , \\text{ otherwise }
        \\end{array}
        \\right.

    Args:
        maximum (int): corresponds to the index of the Pseudo-dirac maximum.
        length (int): Space size :math:`\\mathcal{D}`.
        eps (float): :math:`\\epsilon` value.

    Returns:
        vect (numpy.ndarray): 1D numpy array of distribution probability values.
    """
    vect = eps * np.ones(length)
    vect[maximum] = 1 - (eps * (length - 1))
    return vect


def gaussian(mu, sigma, length):
    u"""Generates a Discretized (truncated) Gaussian distribution of parameters :math:`\\mu` and
    :math:`\\sigma`. This function evaluates probability desity function for integers from 0 to
    :math:`(\\mathcal{D}-1)` and returns these values as a normalized array (sum is equal to 1).

    Args:
        mu (float): :math:`\\mu`, the Gaussian distribution mean
        sigma (float): :math:`\\sigma`, the Gaussian distribution standard deviation.
        length (int): :math:`\\mathcal{D}` Space size.
        norm (bool): Normalized version or not.

    Returns:
        vect (numpy.ndarray): 1D numpy array of distribution probability values.
    """
    vect = np.exp(-np.power(np.arange(0, length) - mu, 2.) /
                  (2 * np.power(sigma, 2.)))
    return vect / np.sum(vect)


def gaussian_cdf(x, mu=0, sigma=1):
    u"""Cumulative distribution function (CDF) of a normal continuous random variable.

    Args:
        x (float): quantile.
        mu (float): mean. Defaults to 0.
        sigma (float): the standard deviation of the normal distribution. Defaults to 1.

    Returns:
         (float): result.
    """
    return norm.cdf((x - mu) / sigma)


def attention_loss(mu, sigma, length):
    u"""Amount of attention distribution (supposed normal and continuous) outside the stimulus .

    Args:
        mu (float): mean value of the distribution (attention focus).
        sigma (float): the standard deviation of the distribution (attention dispersion).
        length (int): stimulus length :math:`N`.

    Returns:
         (float): result.
    """
    loss_left = gaussian_cdf(-0.5, mu, sigma)
    loss_right = 1 - gaussian_cdf(length - 0.5, mu, sigma)
    return loss_left + loss_right


# Other helpers

def ld_control(ldyesvalue):
    u"""A stepped version of the lexical retroaction function.

    Args:
        ldyesvalue (float): lexical decision "YES" answer probability.

    Raises:
        ValueError: an error is raised if the probability value is less than 0 or greater than 1.

    Returns:
        (float): result.
    """
    if (ldyesvalue > 1 and ldyesvalue < 0):
        raise ValueError('Incorrect Lexical Decision value.')

    if ldyesvalue > 0.95:
        return 4e-4
    elif ldyesvalue > 0.90:
        return 6e-5
    elif ldyesvalue > 0.85:
        return 4e-5
    elif ldyesvalue > 0.75:
        return 2e-5
    elif ldyesvalue > 0.65:
        return 1e-5
    elif ldyesvalue > 0.55:
        return 5e-6
    elif ldyesvalue <= 0.55:
        return 2.5e-6


def len2phlen(x):
    u"""Linear function that links orthographic to phonological length.
    This function corresponds to the result of a linear regression of phonological length giver orthographic length
    based on a french lexicon (Lexique 3.82).
    """
    return .12 + (.73 * x)


def norm1D(arr):
    """Normalizes a 1D probability distribution"""
    if arr.sum() > 0:
        return arr / arr.sum()


def norm_percept(p):
    """Normalizes percept distribution"""
    return p / p.sum(axis=1, keepdims=True)


@numba.jit(nopython=True, debug=False)
def norm2D(matrix, n=2, dim=2):
    """Normalizes a 2D probability distribution"""
    if matrix is not None:
        res = np.zeros(matrix.shape)
        for j in range(matrix.shape[0]):
            if n == 2:
                norm = LA.norm(matrix[j, :])
            else:
                norm = np.sum(matrix[j, :])
            if norm > 0:
                for k in range(matrix.shape[1]):
                    res[j, k] = matrix[j, k] / norm
        return res
    return None


@numba.jit(nopython=True, debug=False)
def norm3D(matrix, n=2):
    """Normalizes a 3D probability distribution"""
    res = np.zeros(matrix.shape)
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            if n == 2:
                norm = LA.norm(matrix[i, j, :])
            else:
                norm = np.sum(matrix[i, j, :])
            if norm > 0:
                for k in range(matrix.shape[2]):
                    res[i, j, k] = matrix[i, j, k] / norm
    return res


def l_round(lst, n=3):
    """Rounds up a list of numbers.

    Args:
        lst (list): list of number to round up.
        n (int): Number of decimals to use to round the number. Defaults to 3.

    Returns:
        (list): result.
    """
    return [round(i, n) for i in lst]


def edit_distance(s1, s2, subst_cost=0.4):
    u"""Computes Levenshtein distance between two character strings (`s1` and `s2`).
    """
    len1 = len(s1)
    len2 = len(s2)
    lev = [[i if j == 0 else j if i == 0 else 0 for j in range(
        len2 + 1)] for i in range(len1 + 1)]
    for i in range(len1):
        for j in range(len2):
            lev[i + 1][j + 1] = min(lev[i][j + 1] + 1,
                                    lev[i + 1][j] + 1,
                                    lev[i][j] + subst_cost * abs(s1[i] - s2[j]))
    return lev[len1][len2]


def get_size(obj, seen=None):
    u"""Recursively finds size of objects : for memory investigation."""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    seen.add(obj_id)
    if isinstance(obj, dict):
        size += sum([get_size(v, seen) for v in obj.values()])
        size += sum([get_size(k, seen) for k in obj.keys()])
    elif hasattr(obj, '__dict__'):
        size += get_size(obj.__dict__, seen)
    elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
        size += sum([get_size(i, seen) for i in obj])
    return size


def fullspec(passedFunc, *args, **kwargs):
    u"""Get the names and default values of a Python function’s argument.
    This fucntion returns a dictionnary with argument names as keys and argument values as values."""
    spec = inspect.getfullargspec(passedFunc)
    args_names = spec.args[1:] if spec.args[0] == 'self' else spec.args
    params = dict(zip(args_names, args))
    defaults = dict(zip(args_names[-len(spec.defaults):], spec.defaults))
    for k, v in kwargs.items():
        params[k] = v
    for k in args_names:
        if k not in params:
            if k in defaults:
                params[k] = defaults[k]
            else:
                raise TypeError('missing argument', k)
    return params


def addLoggingLevel(levelName, levelNum, methodName=None):
    u"""Comprehensively adds a new logging level to the `logging` module and the currently configured logging class.
    `levelName` becomes an attribute of the `logging` module with the value `levelNum`.
    `methodName` becomes a convenience method for both `logging` itself and the class returned
    by `logging.getLoggerClass()` (usually just `logging.Logger`). If `methodName` is not specified, `levelName.lower()` is used.

    To avoid accidental clobberings of existing attributes, this method will
    raise an `AttributeError` if the level name is already an attribute of the
    `logging` module or if the method name is already present.

    Example:
        >>> addLoggingLevel('TRACE', logging.DEBUG - 5)
        >>> logging.getLogger(__name__).setLevel("TRACE")
        >>> logging.getLogger(__name__).trace('that worked')
        >>> logging.trace('so did this')
        >>> logging.TRACE
        5

    Note:
        This function is copied from an StackOverflow thread :
        https://stackoverflow.com/questions/2183233/how-to-add-a-custom-loglevel-to-pythons-logging-facility
    """
    if not methodName:
        methodName = levelName.lower()

    if hasattr(logging, levelName):
        raise AttributeError(
            '{} already defined in logging module'.format(levelName))
    if hasattr(logging, methodName):
        raise AttributeError(
            '{} already defined in logging module'.format(methodName))
    if hasattr(logging.getLoggerClass(), methodName):
        raise AttributeError(
            '{} already defined in logger class'.format(methodName))

    def logForLevel(self, message, *args, **kwargs):
        if self.isEnabledFor(levelNum):
            self._log(levelNum, message, args, **kwargs)

    def logToRoot(message, *args, **kwargs):
        logging.log(levelNum, message, *args, **kwargs)

    logging.addLevelName(levelNum, levelName)
    # streams debug logging messages to stdout instead of stderr (no more red
    # background in jupyter)
    logging.basicConfig(stream=sys.stdout)
    setattr(logging, levelName, levelNum)
    setattr(logging.getLoggerClass(), methodName, logForLevel)
    setattr(logging, methodName, logToRoot)


def abstractmethod(method):
    u"""An @abstractmethod member fn decorator."""
    def default_abstract_method(*args, **kwargs):
        raise NotImplementedError('call to abstract method '
                                  + repr(method))
    default_abstract_method.__name__ = method.__name__
    return default_abstract_method
