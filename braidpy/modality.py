#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""modality.py: [a file of the BRAID software] Orthographical and phonological submodules."""
__copyright__ = "2021, CNRS, UGA"
__authors__ = "Diard, Valdois, Phénix, Ginestet, Saghiran, Steinhilber"
__contact__ = "Julien Diard, julien.diard@univ-grenoble-alpes.fr"
__licence__ = "CECILL-2.1"
__date__ = "Aug 23 2021"

import braidpy.utilities as utl

# Scientific/Numerical computing
from scipy.stats import entropy
from numpy import linalg as LA
import numpy as np
import pandas as pd

# General purpose libraries
import copy
import logging
import os


class modality:
    """ The class :obj:`modality` is (private) class that allows to represent syntethically both orthographical and phonological knowledge.
    It gathers knowledge about percepts (letters or phonemes), lexical items (words) and lexical membership (lexical decision). This class is used as an InnerClass of the model class :obj:`braid`.

    Attributes:
        model (:obj:`braid`): model superclass
        mod (str): modality (``ortho`` or ``phono``)
        eps (float): Representation quality :math:`\\eps`
        chars (pandas.DataFrame): character table
        chars_filename (str): characters table filename
        conf_mat (numpy.ndarray): confusion matrix
        conf_mat_name (str): confusion matrix filename
        n (int): number of chars, corresponds to number of letters :math:`Card(\\mathcal{D}_L)` in the orthographic side and the number of phonemes :math:`Card(\\mathcal{D}_\\phi)` in the phonological side
        N (int): length, corresponds to :math:`N` in the orthographic side and :math:`M` in the phonological side
        N_max (int): maximum length value
        learning:
        whole_repr (Dict): lexical knowledge as a probability distribution values (:math:`P(L_{1:N}|W)`) for all possible lengths
        repr (numpy.ndarray): lexical knowledge as a probability distribution values (:math:`P(L_{1:N}|W)`) for the selected length
        dist (Dict): output probability distributions (percept, words and ld)
        wsim (numpy.ndarray): similarity product result :math:`<P(L_n|W)~|~P(P_n|\\textit{input})>`
        top_down_influence (bool): if set to True, top dow influence are allowed
        remove_stim (bool): it set to True,
        exists (bool): if set to True, the submodule exists
        enabled (bool): if set to True, the submodule is enabled
        store (bool): if set to True, output probability distributions are stored
        attention_profile (numpy.array): attention distribution
        stim (str): stimulus
    """

    def __init__(self, model=None, mod="ortho", stim="help", eps=0.001, chars=None,
                 chars_filename=None, conf_mat_name=None, n=None, N=None, N_max=15,
                 repr=None, whole_repr=None, learning=False, dist=None,
                 wsim=None, top_down_influence=True, remove_stim=False,
                 enabled=True, store=True):
        """Object constructor, modality initiator.

            Args:
                see attributes list above.

            Returns:
                self (:obj:`modality`).
        """
        self.model = model
        self.mod = mod
        self.eps = eps
        self.chars_reduced = False
        self.chars = chars
        self.chars_filename = chars_filename
        self.conf_mat = None
        self.conf_mat_name = conf_mat_name
        self.n = n
        self.N = N
        self.N_max = N_max
        self.repr = repr

        # Warning : whole_repr indexé par self.model.ortho.N, car les N ortho
        # sont uniques, pas les phono
        self.whole_repr = whole_repr
        self.learning = learning
        self.dist = dist
        self.wsim = wsim
        self.top_down_influence = top_down_influence
        self.remove_stim = remove_stim
        self.exists = False
        self.enabled = enabled
        self.store = store
        self.attention_profile = None
        self.stim = stim

    @utl.abstractmethod
    def __contains__(self, item):
        return False

    @property
    def repr(self):
        return self._repr

    @repr.setter
    def repr(self, value):
        self._repr = value

    @property
    def stim(self):
        return self._stim

    @stim.setter
    @utl.abstractmethod
    def stim(self, value):
        pass

    ##### INIT BOTTOM UP ###########################

    def set_char_dict(self):
        u"""Loads alphebet or phonetic characters indexes given `self.chars_filename` attribute.
        The file should be a csv file with at least two columns: `idx` column that contain indexes and `char` column that contain character identity
        (for examples, look at files in ``resources/char_dicts``).

        Note:
            Please make sure the files follow a unicode encoding.
        """
        col = ['idx', 'char']  # if mod == "ortho" else ['idx', 'char', 'ipa']
        dict_path = os.path.realpath(os.path.dirname(
            __file__)) + '/../resources/chardicts/' + self.chars_filename
        df = pd.read_csv(dict_path, usecols=col)
        self.chars = "".join(df.char)  # it is simply a string
        self.n = len(self.chars)

    @utl.abstractmethod
    def set_confusion_matrix(self):
        return

    def build_trace(self):
        """Extract from the confusion matrix lines corresponding to phonoemes/letters of the input "input_str"
        If the sign is not a known letter, the corresponding distribution is considered as uniform (probabilistic equivalent of "i don't know")."""
        if self.N > self.N_max:
            raise ValueError(
                "String is longer than known words, cannot handle this case")
        res = np.ones((self.n, self.N))
        for i in range(self.N):
            res[:, i] = np.array(
                self.conf_mat.T[self.chars.index(self.stim[i])])
        return res

    @utl.abstractmethod
    def build_attention_distribution(self):
        """
        """
        return

    # INIT DIST

    def reset_dist(self):
        """Resets probability distributions.
        """
        self.dist["percept"] = np.ones((self.N, self.n)) / self.n
        if self.exists:
            self.dist["ld"] = np.array([0.5, 0.5])
            self.dist["TDI"] = 0
            self.dist["word"] = self.model.freq / sum(self.model.freq)

    # INIT TOP DOWN

    def build_whole_knowledge(self):
        """Returns a dictionnary of 3D matrix of phonological/orthographical memory traces for each word length (whole_repr dictionnary)."""
        if self.enabled:
            self.whole_repr = {
                i: self.build_knowledge(
                    i +
                    1) for i in range(
                    self.N_max)}
            self.exists = (sum([len(self.whole_repr[i])
                           for i in range(self.N_max)]) > 0)  # self stored ?
            # normer directement les représentations lexicales pour le produit
            # scalaire
            if self.model.version in [7, 9] and self.exists:
                self.whole_repr = {
                    k: utl.norm3D(v) if len(v) > 0 else v for k,
                    v in self.whole_repr.items()}

    def build_knowledge(self, n):
        """Returns a 3D matrix of phonological/orthographical memory traces for a specific word length."""
        lex = self.model.lexicon[self.model.lexicon.len == n].reset_index()
        if len(lex) == 0:
            return []
        wds = lex[lex[self.mod] == True]
        if len(wds) == 0:
            return []
        wds_idx = np.array([[self.chars.index(letter) for letter in wd] for wd in
                           np.array(wds["word" if self.mod == "ortho" else "pron"])])
        return utl.create_repr(wds_idx, self.n, self.eps)

    def set_knowledge(self):
        """Sets the current orthographical/phonological knowledge to the length of the current stimulus."""
        if self.remove_stim:
            self.remove_stim_repr()
        self.repr = self.whole_repr[self.model.ortho.N - 1] if self.whole_repr is not None \
            else self.build_knowledge(self.model.ortho.N)
        self.exists = len(self.repr) > 0 and not np.all((self.repr == 0))
        self.N = np.shape(self.repr)[1]

    def remove_stim_repr(self):
        if self.exists:
            raw = self.get_word_entry()
            self.old_repr = copy.copy(
                self.whole_repr[self.model.ortho.N - 1][raw.idx])
            self.whole_repr[self.model.ortho.N -
                            1][raw.idx, :, :] = self.get_empty_percept(0)
            self.model.lexicon.loc[self.model.ortho.stim, self.mod] = False

    def complete_repr(self):
        n = self.model.ortho.N
        if n > 3:
            minusRepr = self.whole_repr[n - 2]
            return minusRepr
        return []

    # RESULT FONCTIONS

    def get_entropy(self, typ="p"):
        u"""Computes entropy of percept and word probabity distributions.

        Args:
            typ (str, optional): `p` for percepts distribution and `w` for word distribution . Defaults to "p".

        Returns:
            numpy.array: entropy value.
        """
        if typ == "p":
            return [entropy(i) for i in self.dist["percept"]]
        elif typ == "w":
            return entropy(self.dist["word"])

    def print_dist(self, dist_name="ld"):
        res = ""
        dist = self.dist[dist_name] if dist_name != 'H' else None
        if dist_name == "percept":
            rg = range(np.shape(dist)[0])
            am = [np.argmax(dist[j]) for j in rg]
            res = self.get_string_chain(am).replace('#', '') + " " + \
                str([round(dist[j][am[j]], 3) for j in rg])
        elif dist_name == 'H':
            res = utl.l_round(self.get_entropy())
        elif self.exists:
            if dist_name == 'ld':
                res = round(dist[0], 4)
            elif dist_name == 'word':
                am = np.argmax(dist)
                phono_name = ('/' + self.model.phono.get_name(am).replace("#", "") + '/') \
                    if 'pron' in self.model.lexicon.columns else ""
                res = f''' {self.model.ortho.get_name(am)}, {phono_name}, wmax = {round(dist[am],4)} '''
        return dist_name + " " + self.mod + ": " + str(res)

    def print_all_dists(self):
        if self.exists:
            logging.simu(self.print_dist("ld"))
            logging.simu(self.print_dist("percept"))
            logging.simu(self.print_dist("word"))

    @utl.abstractmethod
    def get_word_entry(self, string):
        return

    def get_string_chain(self, idx):
        """Gets the string corresponding to the list of indexes."""
        return ''.join([self.chars[i] for i in idx])

    def get_name(self, index):
        """Returns the name of the word corresponding to the given index in `self.ortho.repr` arrays."""
        lx = self.model.lexicon
        try:
            df = lx[(lx.idx == index) & (lx.len == self.model.ortho.N)]
            return df.index[0] if self.mod == "ortho" else list(df.pron)[0]
        except IndexError:
            logging.exception("Word index not found.")

    @utl.abstractmethod
    def get_freq(self, name):
        return

    @utl.abstractmethod
    def get_index(self, name):
        return

    def get_most_probable_word(self, n=1, dist="word"):
        """Returns the name of the most probable word."""
        if self.exists:
            l = [self.get_name(i)
                 for i in np.argsort(self.dist[dist])[::-1][:n]]
            return l if n > 1 else l[0]

    ########### DISTRIBUTION CALCULATION #########

    @utl.abstractmethod
    def one_step_modality(self):
        return

    def get_empty_percept(self, value):
        u = np.empty((self.N, self.n))
        u.fill(value)
        return u

    def update_similarity(self):
        """Updates the the similarity between :math:`P` variables and :math:`W` variables."""
        # similarité calculée pour chaque mot -> permet de calculer la reco -> nb_wds*N
        # TODO ali garder seulement version 9 (enlever l'attribut version)
        wtrans = utl.build_word_transition_vector(
            self.dist["word"], self.model.freq, self.model.leakW)
        wsim = utl.wsim(self.repr, self.dist["percept"])
        self.wsim = np.array(wtrans)[:, np.newaxis] * wsim
        return wtrans

    def update_word_distribution(self):
        """Updates the word distribution"""
        self.dist["word"] = self.wsim[:, 0] / self.wsim[:, 0].sum(0)

    def update_ld_distribution(self):
        """Updates the lexical decision distribution"""
        proba_error_i = self.wsim.sum(0)

        if self.model.version in [0]:
            proba_ld = np.array([proba_error_i[0],
                                 np.mean(proba_error_i[1:]) / (self.n - 1)])
        elif self.model.version in [7, 9]:
            # calculs équivalents mais deuxième version + rapide
            # proba_ok=[1]+[LA.norm(i)/LA.norm(1-i) for i in self.dist["percept"]]
            proba_ok = [1] + [1 / np.sqrt(1 + (self.n - 2) / LA.norm(i) ** 2)
                              for i in self.dist["percept"]]
            proba_err = [i * j for i, j in zip(proba_ok, proba_error_i)]
            proba_ld = np.array([proba_err[0], np.mean(proba_err[1:])])
        else:
            raise ValueError(
                "LD version value not implemented. Choose between 0(vanilla LD), 7(L2 LD) and 9(L2+Logfreq LD).")

        ld_proba_trans = np.matmul(
            self.model.ld_trans_mat,
            self.dist["ld"]) * proba_ld  # Markov transition
        self.dist["ld"] = ld_proba_trans / ld_proba_trans.sum()

    @utl.abstractmethod
    def update_percept(self):
        return


class orthography(modality):
    """Orthographic modality."""

    def __init__(self, crowding=0.675, scaleI=5.8, slopeG=1, confFactor=1, leakP=1e-4,
                 leakW=1250, meanA=-1, gazePosition=-1, Qa=1, sdA=1.75, sdM=5, conf_mat_name=None,
                 **modality_args):
        super().__init__(**modality_args)
        self.crowding = crowding
        self.scaleI = scaleI
        self.slopeG = slopeG
        self.Qa = Qa
        self.confFactor = confFactor
        self.leakP = leakP
        self.conf_mat_name = conf_mat_name
        self.interference_mat = None
        self.bottom_up_matrix = None

    def __contains__(self, item):
        return item in self.model.lexicon.index and self.model.lexicon.loc[item].ortho == True

    @modality.stim.setter
    def stim(self, value):
        """Stimulus setter. It affects also the new value for the N attribute."""
        self._stim = value if isinstance(value, str) else ""
        self.N = len(self.stim)
        if self.exists:
            self.model.init_bottom_up_process()

    # BOTTOM UP #########################"

    def set_char_dict(self):
        u"""Loads alphebet or phonetic characters indexes given `self.chars_filename` attribute.
        The file should be a csv file with at least two columns: `idx` column that contain indexes and `char` column that contain character identity
        (for examples, look at files in ``resources/char_dicts``).

        Note:
            Please make sure the files follow a unicode encoding.
        """
        if len(self.chars_filename) == 0:
            self.chars_filename = "alphabet_en.csv" if self.model.langue == "en" else "alphabet_lat.csv"
        if self.model.langue == "fr" and self.chars_reduced:
            self.chars_filename = "alphabet_fr_accents.csv"
        super().set_char_dict()

    def build_bottom_up_ortho(self):
        self.build_interference_matrix()
        self.build_attention_distribution()
        if self.model.meanA >= 0:
            self.bottom_up_matrix = [i * a + (1 - a) / self.n for (i, a) in
                                     zip(self.interference_mat, self.attention_profile)]

    def set_confusion_matrix(self):
        u"""Loads the letter confusion matrix given `self.conf_mat_name` attribute. The file should be a csv file representing a square matrix
        (for examples, look at files in ``resources/confusionMatrix``).
        """
        if len(self.conf_mat_name) == 0:
            self.conf_mat_name = "TownsendMod.xls" if self.model.langue == "en" else "Simpson13Mod.xls"
        dict_path = os.path.realpath(os.path.dirname(
            __file__)) + '/../resources/confusionMatrix/' + self.conf_mat_name
        self.conf_mat = pd.read_excel(dict_path, header=None)

    def build_interference_matrix(self):
        """ This function implements the low-level lettre perception mechanisms :
        Letter confusion, Acuity gradient and Crowding."""
        # Sensory distribution based on stimulus identity and the considered
        # letter confusion matrix
        input_mat = self.build_trace()

        acuity_mat = [(input_mat[..., x] +
                       self.scaleI +
                       abs(self.model.gazePosition -
                           x) *
                       self.slopeG) /
                      (1 +
                       self.n *
                       (self.scaleI +
                        abs(self.model.gazePosition -
                            x) *
                           self.slopeG)) for x in np.arange(input_mat.shape[1])]

        crowding_mat = np.array([[self.crowding if i == j else (1 - self.crowding) / 2
                                  if abs(i - j) == 1 else 0 for j in range(self.N)] for i in range(self.N)])
        crowding_mat = crowding_mat / crowding_mat.sum(axis=1)[:, np.newaxis]

        self.interference_mat = crowding_mat @ acuity_mat

    def build_attention_distribution(self):
        """Returns attention distribution given length, focus (:math:`\\mu_A`),
        amount of attention :math:`Q_A` and dispersion (:math:`\\sigma_A`)."""
        if self.model.meanA >= 0:
            tmp = utl.gaussian(
                self.model.meanA, self.model.sdA, len(
                    self.stim))
            tmp = self.Qa * tmp
            tmp[tmp > 1] = 1
            self.attention_profile = tmp / tmp.sum()
        else:
            self.attention_profile = None

    def build_percept(self):
        """Implements letter Perceptual information accumulation."""
        tmp = (self.dist["percept"] + self.leakP) / (1 + self.n * self.leakP)
        self.dist["percept"] = utl.norm_percept(tmp * self.bottom_up_matrix)

    def get_freq(self, name):
        try:
            return float(self.model.lexicon.loc[name].freq)
        except KeyError:
            logging.exception("String not found in lexicon")

    def get_index(self, name):
        """Returns the index of the word in representation (repr) arrays."""
        name = name if name is not None else self.stim
        try:
            return int(self.model.lexicon.loc[name].idx)
        except KeyError:
            logging.exception("String not found in lexicon")

    def get_word_entry(self, string=""):
        """Returns the corresponding word entry in the lexicon."""
        string = string if len(string) > 0 else self.stim
        return self.model.lexicon.loc[string] if string in self.model.lexicon.index else None

    ########### DISTRIBUTION CALCULATION #############
    def update_similarity(self):
        wtrans = super().update_similarity()

    def one_step_modality(self):
        if self.exists:  # "exists = false" means empty lexicon => nothing to compute
            self.update_similarity()
            self.update_word_distribution()
            self.update_ld_distribution()

    def update_percept(self):
        """ update the percept distribution with the top down retroaction"""
        # Only orthographic lexical knowledge influence letter perception
        for mod in ["ortho"]:
            data = getattr(self.model, mod)
            if data.exists and data.top_down_influence:
                # sigmoid top-down influence -- Ali's version
                data.dist["TDI"] = 2e-6 + 1 * \
                    (5e-4 / np.power((1. +
                     np.exp(-(97 * data.dist["ld"][0]) + 95)), .3))
                TD_dist = np.einsum('i,ijk->jk', data.dist["word"], self.repr)
                self.dist["percept"] = utl.norm_percept((data.dist["TDI"] * TD_dist + (1 / self.n) * (
                    1 - data.dist["TDI"]) * np.ones(self.dist["percept"].shape)) * self.dist["percept"])


class phonology(modality):
    """
    Phonological modality.
    """

    def __init__(self, leakPhon=0.05, QPhon=1, **modality_args):
        super().__init__(**modality_args)
        self.leakPhon = leakPhon
        self.QPhon = QPhon
        self.pron = None
        self.pron_idx = None
        self.ipa = None

    def __contains__(self, item):
        lx = self.model.lexicon
        return item in list(lx.pron) and self.get_word_entry(
            item).phono == True

    @modality.stim.setter
    def stim(self, value):
        self._stim = value
        self.N = len(self._stim)

    def set_confusion_matrix(self):
        """No effect. No confusion matrice at the phonological level.
        """
        self.conf_mat = np.eye(self.n)

    def set_char_dict(self):
        u"""Loads alphebet or phonetic characters indexes given `self.chars_filename` attribute.
        The file should be a csv file with at least two columns: `idx` column that contain indexes and `char` column that contain character identity
        (for examples, look at files in ``resources/char_dicts``).

        Note:
            Please make sure the files follow a unicode encoding.
        """
        if len(self.chars_filename) == 0:
            self.chars_filename = "xsampa_celex.csv" if self.model.langue in "en" else \
                "xsampa_fr.csv" if self.model.langue in "fr" else "xsampa_celex_german.csv"
        super().set_char_dict()

        dict_path = os.path.realpath(os.path.dirname(
            __file__)) + '/../resources/chardicts/' + self.chars_filename
        df = pd.read_csv(dict_path, usecols=["ipa"])
        self.ipa = df.ipa.values

    def build_attention_distribution(self):
        """Returns phono attention distribution given length, mean position, Qa
        and standard deviation"""
        if self.model.meanA >= 0:
            self.attention_profile = utl.gaussian(utl.len2phlen(self.model.meanA),
                                                  utl.len2phlen(
                                                      self.model.sdA),
                                                  self.N)
        else:
            self.attention_profile = None

    def one_step_modality(self):
        """ This feature is not implemented.
        The lexicon is considered unique and not splitted into a phonological lexicon and an orthographic lexicon.
        Each word of the lexicon has an orthographic representation and phonological one and a probability value evolving through iterations.
        """
        self.dist["word"] = self.model.ortho.dist["word"]
        return

    def update_percept(self):
        """Updates the phonemes identity distribution ():math:`\\psi` varioables in the model)"""
        repr = self.repr
        ph_tmp = utl.norm_percept(
            np.einsum(
                'i,ijk->jk',
                self.dist["word"],
                repr))
        # Modulation/Filtering of lexical information
        filt_ph = utl.norm_percept(np.array(
            [i * a * self.QPhon + (1 - a * self.QPhon) / self.n for (i, a) in zip(ph_tmp, self.attention_profile)]))

        # Phonological STM and output distribution
        mem_ph = (self.dist["percept"] + self.leakPhon) / \
            (1 + self.n * self.leakPhon)
        self.dist["percept"] = utl.norm_percept(mem_ph * filt_ph)

    def print_all_dists(self):
        super().print_all_dists()
        if self.exists:
            logging.simu(f"generated phonological form: /{self.pron}/")

    def get_word_entry(self, string=None):
        string = string if string is not None else self.stim
        return self.model.lexicon[self.model.lexicon.pron == string].iloc[0]

    def get_freq(self, name=None):
        name = name if name is not None else self.stim
        try:
            lx = self.model.lexicon
            return float(self.get_word_entry(name).freq)
        except KeyError:
            logging.exception("Phono String not found in lexicon")

    def get_index(self, name=None):
        """Returns the index of the word in self.ortho.repr arrays."""
        name = name if name is not None else self.stim
        try:
            lx = self.model.lexicon
            return int(self.get_word_entry(name).idx)
        except KeyError:
            logging.exception("Phono String not found in lexicon")
