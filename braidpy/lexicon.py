#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""lexicon.py: [a file of the BRAID software] lexicon manipulations helpes."""
__copyright__ = "2021, CNRS, UGA"
__authors__ = "Diard, Valdois, Phénix, Ginestet, Saghiran, Steinhilber"
__contact__ = "Julien Diard, julien.diard@univ-grenoble-alpes.fr"
__licence__ = "CECILL-2.1"
__date__ = "Aug 23 2021"

import pandas as pd
import sys, os
sys.path.append("../")
sys.path.append("../../braidpy")

# temporary pseudoword list
MPM = ["apper", "dind", "fies", "gred", "proot", "sahs", "stal", "tive", "tus"]


def selectdf(df, lenMin, lenMax, maxItem, maxItemLen):
    """Selects words with specific lengths from lexicon.

    Args:
        df (pandas.DataFrame): original lexicon
        lenMin (int): minimum length
        lenMax (int): maximum length
        maxItem (int): output lexicon length (number of rows)
        maxItemLen (int): maximum word length

    Returns:
        (pandas.DataFrame): filtered lexicon.
    """
    df = df[(df['len'] >= lenMin) & (df['len'] <= lenMax)].set_index('word')
    if maxItemLen is not None and maxItemLen < maxItem:
        df = df.groupby('len').apply(lambda x: x.sample(n=maxItemLen, random_state=25)
                                     if x.shape[0] >= maxItemLen else x).droplevel(0)
    # on garde word comme index
    return df.head(maxItem)


def extractLexicon(path="../", lexicon_name="BLP.csv", maxItem=100000, maxItemLen=None, lenMin=3,
                   lenMax=13, fMin=0, fMax=1000000, cat=None, phono=False, ortho=True, forbid_list=None, return_df=False):
    """Extract lexicon dataframe given lexicon file name.
    To use new lexicon please follow the same structure of existant lexicon (``resources.lexicon``).
    The csv can at least contains ``word`` , ``freq`` and ``len`` columns.
    To simulate reading it must contain also a ``pron`` (pronounciation) and ```phlen`` (phonological length) columns.

    Args:
        path (str, optional): Defaults to "../".
        lexicon_name (str, optional): Defaults to "BLP.csv".
        maxItem (int, optional): Defaults to 100000.
        maxItemLen ([type], optional): Defaults to None.
        lenMin (int, optional): Defaults to 3.
        lenMax (int, optional): Defaults to 13.
        fMin (int, optional): Minimum frequency. Defaults to 0.
        fMax (int, optional): Maximum frequency. Defaults to 1000000.
        cat ([type], optional): Defaults to None.
        phono (bool, optional): if True, phonology exists. Defaults to False.
        ortho (bool, optional): if True, orthography exists. Defaults to True.
        forbid_list ([type], optional): words to forbid. Defaults to None.
        return_df (optional): output DataFrame. Defaults to False.

    Returns:
        (dataFrame): output DataFrame.
    """
    # attention si change lenMax, ça change les mots chosit, on peut pas augmenter au fur et à mesure les len
    # par contre on peut augmenter maxItemLen
    df = pd.read_csv(os.path.realpath(os.path.dirname(__file__)) + '/../resources/lexicon/' + lexicon_name,
                     keep_default_na=False)
    # choisit la catégorie grammaticale si demandé
    if cat is not None and 'cat' in df.columns:
        df = df[df.cat == cat]
    df["word"] = df.word.str.lower().replace("'", "").replace("-", "")
    col = ['len', 'word', 'freq'] + \
        (['pron', 'phlen'] if phono and 'pron' in df.columns else [])
    # gestion des homographes
    agg_fun = {key: 'first' for key in col}
    agg_fun['freq'] = 'sum'
    agg_fun['len'] = 'min'
    df = df.groupby('word').agg(agg_fun).reset_index(drop=True)
    df = df.assign(ortho=ortho).assign(phono=(phono & ('pron' in df.columns)))
    if forbid_list is not None:
        df = df[~df.word.isin(forbid_list)]
    df = df[(df['freq'] < fMax) & (df['freq'] > fMin)]
    df = selectdf(
        df,
        lenMin,
        lenMax,
        maxItem,
        maxItemLen)  # .to_dict()['freq']
    return df if return_df else list(df.reset_index().word)
