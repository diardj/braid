#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""braid_plot.py: [a file of the BRAID software] Local plotting library."""
__copyright__ = "2021, CNRS, UGA"
__authors__ = "Diard, Valdois, Phénix, Ginestet, Saghiran, Steinhilber"
__contact__ = "Julien Diard, julien.diard@univ-grenoble-alpes.fr"
__licence__ = "CECILL-2.1"
__date__ = "Aug 23 2021"

# data manipulation
import pandas as pd
import numpy as np

# plotting lib
import seaborn as sns                   # awsom stat plots
import matplotlib.pyplot as plt

sns.set_style("whitegrid")


def plot_confusion_matrix(model, title=""):
    """Plots confusion matrix.

    Args:
        model (:obj:`model`): BRAID model (since it contains all the necessary information).
        title (str, optional): Figure title. Defaults to "".
    """
    labels = list(model.orth_chars.keys())
    # labels.pop()

    plt.figure(figsize=(25, 25))
    a = model.conf_mat
    fig, ax = plt.subplots(figsize=(8, 7))
    im = ax.imshow(a, cmap='viridis', interpolation='nearest')
    fig.colorbar(im)
    for axis in [ax.xaxis, ax.yaxis]:
        axis.set(ticks=np.arange(0, len(labels)), ticklabels=labels)

    plt.title("Letter identity Confusion Matrix \n" + title)
    plt.show()
    return


def plot_word_distribution(model,
                           dist_mat,
                           item_to_show=10,
                           title="Word distribution",
                           xlabel="Words",
                           ylabel="Probability",
                           save_figure=False):
    """Plots word probability distribution.

    Args:
        model (:obj:`model`): BRAID model (since it contains all the necessary information).
        dist_mat (numpy.ndarray): Word probability distribution
        item_to_show (int, optional): number of words to plot. Defaults to 10.
        title (str, optional): Figure title. Defaults to "Word distribution".
        xlabel (str, optional): X axis label. Defaults to "Words".
        ylabel (str, optional): Y axis label. Defaults to "Probability".
        save_figure (bool, optional): Defaults to False.
    """
    current_palette = sns.color_palette("PuBuGn_d", item_to_show)
    to_plot = np.argsort(dist_mat)[::-1][0:item_to_show]
    plt.figure(figsize=(20, 5))
    sns.barplot(model.word_list.reset_index().loc[to_plot, "word"],
                np.sort(dist_mat)[::-1][0:item_to_show],
                palette=current_palette)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.show()
    if save_figure:
        plt.savefig('foo.png')
    return


def plot_word_distribution_evo(model, dist_mat, item_to_show=10,
                               save_figure=False,
                               stimulus="_unknown_",
                               fig_title="Word recognition",
                               fig_sz=None):
    """ Plots word probability distribution evolution curves.

    Args:
        model (:obj:`model`): BRAID model (since it contains all the necessary information).
        dist_mat (numpy.ndarray): word probability distribution evolution data.
        item_to_show (int, optional): number of words to plot. Defaults to 10.
        save_figure (bool, optional): Defaults to False.
        stimulus (str, optional): stimulus char string. Defaults to "_unknown_".
        fig_title (str, optional): figure title. Defaults to "Word recognition".
        fig_sz (tuple, optional): figure size. Defaults to None.
    """

    current_palette = sns.color_palette("tab20", item_to_show)
    idx_to_plot = np.argsort(dist_mat.mean(1))[::-1][0:item_to_show]
    tmplst = model.lexicon[model.lexicon.len == 5].reset_index().word.to_list()
    wrd_to_plot = [tmplst[i] for i in idx_to_plot]

    wr_res_mat = dist_mat
    tmp = wr_res_mat.flatten('F')
    res = pd.DataFrame(
        {"Probability": tmp,
         "Word": np.tile(tmplst, wr_res_mat.shape[1]),
         "idx": np.tile(np.arange(len(tmplst)), wr_res_mat.shape[1]),
         "Iterations": np.repeat(np.arange(wr_res_mat.shape[1]), wr_res_mat.shape[0])
         })

    other_df = pd.DataFrame({
        "Probability": res[~res.idx.isin(idx_to_plot)].groupby(by="Iterations").max().Probability,
        "Iterations": np.arange(res.Iterations.max() + 1)
    }
    )
    other_df['Word'] = "Max of the rest"

    data = res[res.idx.isin(idx_to_plot)]

    if fig_sz is not None:
        # sns.set(rc={'figure.figsize': fig_sz})   #Fig size
        plt.figure(figsize=fig_sz)

    g = sns.lineplot(data=data, x="Iterations", y="Probability", hue="idx",
                     hue_order=idx_to_plot, palette=current_palette, ci=False)
    plt.legend(title='Words', labels=wrd_to_plot, ncol=4,
               loc='lower center', bbox_to_anchor=(.5, -.55))
    plt.ylim(bottom=0, top=data.Probability.max() + .05)
    plt.xlim(left=0, right=data.Iterations.max())
    i = 0
    plt.fill_between(np.arange(data.Iterations.max() + 1),
                     0, other_df['Probability'], alpha=0.5)

    plt.title(fig_title)
    plt.ylabel("Word identity probability")
    plt.xlabel("Iterations")
    plt.show()
    if save_figure:
        plt.savefig('w_evo.pdf')
    return res


def plot_phoneme_distribution_evo(model, dist_mat,
                                  stimulus="_unknown_", item_to_show=4,
                                  save_figure=False, col_wrap=4, pos2plot=0,
                                  startat=0, fig_sz=None, fig_title=" "):
    """Plots phoneme identity distributions over time for each position.

    Args:
        model (:obj:`braid`): BRAID model (since it contains all the necessary information).
        dist_mat (numpy.ndarray): Phoneme identity distribution, (axis0 for positions, axis1 for phonemes and axis2 for iterations).
        stimulus (str, optional): stimulus char string. Defaults to "_unknown_".
        item_to_show (int, optional): number of phonemes to plot per subplot. Defaults to 4.
        save_figure (bool, optional): sefaults to False.
        col_wrap (int, optional): number of subplots per row (only available for plotting multiple positions). Defaults to 4.
        pos2plot (int, optional): phonological position to plot (-1 corres^ponds to plotting all positions). Defaults to 0.
        startat (int, optional): index of the first phoneme (O or 1). Defaults to 0.
        fig_sz (tuple, optional): figure size. Defaults to None.
        fig_title (str, optional): figure title. Defaults to " ".
    """

    dist_mat = np.swapaxes(dist_mat, 0, 1)
    tmp = dist_mat.flatten('C')  # C-like representation of tables
    phon = np.repeat(
        list(
            model.phono.chars),
        dist_mat.shape[1] *
        dist_mat.shape[2])
    pos = np.tile(
        np.repeat(
            np.arange(
                dist_mat.shape[1]),
            dist_mat.shape[2]),
        dist_mat.shape[0])
    t = np.tile(
        np.arange(
            0,
            dist_mat.shape[2]),
        dist_mat.shape[0] *
        dist_mat.shape[1])

    # make a  dataframe
    ph_df = pd.DataFrame({"Probability": tmp,
                          "Phoneme": phon,
                          "Position": pos,
                          "Iterations": t}
                         )

    # phonemes_to_plot
    ph_df['plot'] = False
    x = (ph_df.groupby(['Phoneme', 'Position'])['Probability'].sum().reset_index()
         .sort_values('Probability', ascending=False)
         .groupby("Position")['Phoneme']
         .unique()
         .reset_index()['Phoneme']
         .apply(lambda x: x[:item_to_show])
         )

    for i in range(x.shape[0]):
        ph_df.loc[(ph_df.Position == i) & (
            ph_df.Phoneme.isin(x[i])), 'plot'] = True

    ph_df["Position"] += startat

    if fig_sz is not None:
        # sns.set(rc={'figure.figsize': fig_sz})   #Fig size
        plt.figure(figsize=fig_sz)

    if pos2plot == -1:
        g = sns.FacetGrid(ph_df.loc[ph_df["plot"] == True],
                          col="Position", col_wrap=col_wrap, aspect=1.5)
        g.map(sns.lineplot, "Iterations", "Probability", "Phoneme", ci=False)
        plt.suptitle(fig_title, y=1.05)
        g.set_xlabels("Iterations")
        g.set_ylabels("Probability")
        g.set(ylim=(0, ph_df.Probability.max() + .05),
              xlim=(0, ph_df.Iterations.max()))

        for ax in g.axes.ravel():
            handles, labels = ax.get_legend_handles_labels()
            ax.legend(handles=handles[0:],
                      labels=list(
                          model.phono.ipa[[model.phono.chars.index(l) for l in labels[0:]]]),
                      title="Phoneme")
    else:
        # Non plotted curves in the considered position
        tmp_df = ph_df.loc[(ph_df['plot'] == False) &
                           (ph_df['Position'] == pos2plot)]
        g = sns.lineplot(data=ph_df.loc[(ph_df['plot'] == True) & (ph_df['Position'] == pos2plot)],
                         x="Iterations", y="Probability", hue="Phoneme", ci=False)
        plt.ylim(bottom=0, top=1.05)
        plt.xlim(left=0, right=ph_df.Iterations.max())
        plt.fill_between(np.arange(tmp_df.Iterations.max() + 1), 0,
                         tmp_df.groupby('Iterations').max()['Probability'],
                         alpha=0.5)
        plt.title(fig_title + "\n Position %d" % pos2plot, y=1.05)
        handles, labels = g.get_legend_handles_labels()
        g.legend(handles, list(
            model.phono.ipa[[model.phono.chars.index(l) for l in labels]]))
    plt.show()

    if save_figure:
        # TODO : figure name
        plt.savefig('phon_evo.pdf')

    return ph_df


def plot_letter_distribution_evo(model,
                                 dist_mat,
                                 stimulus="_unknown_",
                                 item_to_show=4,
                                 save_figure=False,
                                 col_wrap=4,
                                 pos2plot=1,
                                 startat=0,
                                 fig_sz=None,
                                 fig_title="Letter identification"):
    """Plots phoneme identity distributions over time for each position.

    Args:
        model (:obj:`braid`): BRAID model (since it contains all the necessary information).
        dist_mat (numpy.ndarray): Letter identity distribution (axis0 for positions, axis1 for letters and axis2 for iterations)
        stimulus (str, optional): stimulus char string. Defaults to "_unknown_".
        item_to_show (int, optional): number of phonemes to plot per subplot. Defaults to 4.
        save_figure (bool, optional): sefaults to False.
        col_wrap (int, optional): number of subplots per row (only available for plotting multiple positions). Defaults to 4.
        pos2plot (int, optional): phonological position to plot (-1 corresponds to plotting all positions). Defaults to 0.
        startat (int, optional): index of the first phoneme (O or 1). Defaults to 0.
        fig_sz (tuple, optional): figure size. Defaults to None.
        fig_title (str, optional): figure title. Defaults to " ".
    """
    dist_mat = np.swapaxes(dist_mat, 0, 1)
    tmp = dist_mat.flatten('C')
    ltr = np.repeat(
        list(
            model.ortho.chars),
        dist_mat.shape[1] *
        dist_mat.shape[2])
    pos = np.tile(
        np.repeat(
            np.arange(
                dist_mat.shape[1]),
            dist_mat.shape[2]),
        dist_mat.shape[0])
    t = np.tile(
        np.arange(
            0,
            dist_mat.shape[2]),
        dist_mat.shape[0] *
        dist_mat.shape[1])

    # make a  dataframe
    ltr_df = pd.DataFrame({"Probability": tmp,
                           "Letter": ltr,
                           "Position": pos,
                           "Iterations": t}
                          )

    # Letters_to_plot
    ltr_df['plot'] = False
    x = (ltr_df.groupby(['Letter', 'Position'])['Probability'].sum().reset_index()
         .sort_values('Probability', ascending=False)
         .groupby("Position")['Letter']
         .unique()
         .reset_index()['Letter']
         .apply(lambda x: x[:item_to_show])
         )

    for i in range(x.shape[0]):
        ltr_df.loc[(ltr_df.Position == i) & (
            ltr_df.Letter.isin(x[i])), 'plot'] = True

    ltr_df["Position"] += startat

    if fig_sz is not None:
        # sns.set(rc={'figure.figsize': fig_sz})   #Fig size
        plt.figure(figsize=fig_sz)

    if pos2plot == -1:
        g = sns.FacetGrid(ltr_df.loc[ltr_df["plot"] == True],
                          col="Position", col_wrap=col_wrap,
                          aspect=1.5)
        g.map(sns.lineplot, "Iterations", "Probability", "Letter", ci=False)
        plt.suptitle(fig_title, y=1.05)
        g.set_xlabels("Iterations")
        g.set(ylim=(0, ltr_df.Probability.max() + .05),
              xlim=(0, ltr_df.Iterations.max()))
        for ax in g.axes.ravel():
            ax.legend()
        # loc='upper left',bbox_to_anchor=(1,0.5))

    else:
        tmp_df = ltr_df.loc[(ltr_df['plot'] == False) &
                            (ltr_df['Position'] == pos2plot)]
        sns.lineplot(data=ltr_df.loc[(ltr_df['plot'] == True) & (ltr_df['Position'] == pos2plot)],
                     x="Iterations", y="Probability", hue="Letter", ci=False)
        plt.ylim(bottom=0, top=1.05)
        plt.xlim(left=0, right=ltr_df.Iterations.max())
        plt.fill_between(np.arange(tmp_df.Iterations.max() + 1), 0,
                         tmp_df.groupby('Iterations').max()['Probability'],
                         alpha=0.5)
        plt.title(fig_title + "\n Position %d" % pos2plot, y=1.05)
        plt.xlabel("Iterations")
        plt.legend()
        plt.show()

    if save_figure:
        # TODO : figure name
        plt.savefig('let_evo.pdf')

    return ltr_df


def plot_lexical_decision_evo(
        model, dist_mat, stimulus, fig_title="Lexical decision", save_figure=False):
    """Plot lexical decision probability evolution curve.

    Args:
        model (:obj:`braid`): BRAID model (since it contains all the necessary information).
        dist_mat (numpy.ndarray): lexical decision distribution evolution (axis0 for YES/NO answers, axis1 iterations)
        stimulus (str): stimulus.
        fig_title (str, optional): figure title. Defaults to "Lexical decision".
        save_figure (bool, optional): Defaults to False.
    """
    timesteps = np.arange(dist_mat.shape[1]) + 1
    # plt.figure(figsize=(7,5))
    plt.plot(timesteps, dist_mat.T)
    plt.ylim(bottom=-0.05, top=1.05)
    plt.xlim(left=0, right=timesteps.max())
    plt.legend(["yes", "no"])
    plt.title(fig_title, y=1.05)
    plt.xlabel("Iterations")
    plt.ylabel("Decision probability")
    if save_figure:
        # TODO : figure name
        plt.savefig('dl_evo.pdf')
    return
