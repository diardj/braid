
# What is BRAID?

This is a Python implementation of the BRAID model. BRAID stands for "Bayesian word Recognition with Attention, Interference and Dynamics". It is a probabilistic model aiming at simulating visual tasks such as letter recognition, isolated word recognition, lexical decision (i.e., deciding whether an input letter string is a known word or not), and reading aloud. 

BRAID's authors are Julien Diard, Sylviane Valdois, Thierry Phénix, Emilie Ginestet, Ali Saghiran and Alexandra Steinhilber. BRAID has been used in several research papers and PhD theses. The main entry points are (Phénix, 2018; Phénix et al., 2018; Ginestet et al., 2019; Saghiran et al., 2020).

**Readme roadmap:** 
1. Use this readme to install the BRAID model and verify that it runs on your system (with a single simulation with bare-bones text ouptut). 
2. To run the BRAID model on a given stimulus, and see resulting plots (to visualize evolutions of probability distributions over time), follow the "demo_func.ipynb" Python notebook in the "notebooks" sub-directory. 
3. Finally, to run the BRAID model on a series of stimuli, and verify that the model yields the classical frequency effect, follow the "demo_freqeffect.ipynb" Python notebook in the "notebooks" sub-directory.

# 1. Software dependencies and installation 

BRAID is developed in Python, with both scripts and notebooks. Make sure you have a properly installed Python environment on your computer. Otherwise, if you are new to Python, please refer to https://www.python.org/downloads/ or, better yet, https://www.anaconda.com/products/individual.

Make sure the installed version of your Python interpreter is >= 3.8.8. Here are the packages needed:
- **Numeric & statistics:** numpy, pandas, scikit-learn, scipy
- **Visualisation & plots:** matplotlib, seaborn
- **Acceleration (optional):** numba
- **Others:** pickle (to save python data structures), xlrd (read excel files)

## 1.1. Installation of packages

### Using pip or conda command line

To install the needed packages, you can use pip (through command-line):

```
$ pip install numpy pandas scikit-learn scipy matplotlib seaborn numba xlrd pickle
```

or Conda (either through the anaconda GUI or command-line):

```
$ conda install numpy pandas scikit-learn scipy matplotlib seaborn numba xlrd
```

`pickle` seems to be a default package in conda and miniconda environments.

### Using the conda Environment loading feature (unstable)

If you want to directly use the conda environment with the appropriate Python/libraries version, you can use the environment file braid_env_09092020.yml

To create the Braid environment, run the following command (after installing conda) in the braid root directory: 
```
$ conda env create -f env/braid_env040221.yml   
```
It will create a new environment named braid_env.

To make this environment active, run the following command: 
```
$ conda activate braid_env
```

## 1.2. Installation of BRAID itself

Download to your computer the BRAID repository you are currently looking at (either by "cloning" it with git, or by manually downloading it). Note in which directory this local version is located, in your file system.

# 2. How to use BRAID

## 2.1. Startup

### If you didn't install the package
To correctly load the BRAID Python code, start any Python script or notebook with:
```python
import sys
sys.path.append("path/to/repository/folder")
```
In the last command, "path/to/repository/folder" should of course be replaced with the actual path to the folder, in your disk, that contains the repository (i.e., the BRAID repository you are currently looking at). 

If you are unsure if the path you typed was correct: unfortunately, the `sys.path.append` command simply adds the given path in a list somewhere, without any error message if, e.g., there was a typo, or you forgot the initial "C:" on PC (or whatever your disk name is), or use forward slashes instead of backward slashes, etc. To verify that the path was correct, you can instead try to list the contents of the directory:

```python
import os
print(os.listdir("path/to/repository/folder"))
```

If the path was correctly typed, Python returns the list of files and sub-folders contained in the BRAID folder. Otherwise, Python will return an error message.

### If you installed the package (see below for this optional installation step)

Nothing to do.

## 2.2. Import classes 

There are 2 main classes:
- the "braid" class, which is the model class
- the "simu" class, which allows running "simulations" (using braid) and storing their results (useful for performing experiments with BRAID on large databases of stimuli, comparing conditions, etc.)

Here, import "simu" in your script or notebook with:
```python
from braidpy.simu import simu
```

## 2.3. Initialize simulation and model classes

Next, configure a simulation of the model on one stimulus ("stim" parameter, here, the English word "help"), for a given number of iterations ("max_iter" parameter, here set to 1000), with all model parameters set to their default values (e.g., the lexicon and letter confusion matrices are the usual ones for English ("en") simulations). To do so, use this command:
```python
sim = simu( max_iter = 1000,  
            stim = "help",
            langue = "en", 
            lexicon_name = "",  
            conf_mat_name = "",  
            ortho_char_name = "",
            phono_char_name = "",
            enable_phono = True,
            store_phono = True,  
            store_ortho = True,  
            model = None,  
            use_pkl = False,  
            save_pkl = False,
            build_prototype = False
            )
model=sim.model
```

We show how to explore the list of parameters, how to see or set their values, in order to play around with this simple case, in the "demo_func.ipynb" notebook demonstration.

## 2.4. Running the simulation

Now, we "run" the simulation, that is, we compute the evolutions of all probability distributions in the model for all iterations, with the command:
```python
sim.run_simu_normal()
```

The expected text output should look like this:
```
SIMU:root:stimulus: help
SIMU:root:simulation duration: 1000
SIMU:root:ld ortho: 0.9865
SIMU:root:percept ortho: help [0.931, 0.896, 0.86, 0.802]
SIMU:root:word ortho:  help, /hElp/, wmax = 0.9924 
SIMU:root:ld phono: 0.5
SIMU:root:percept phono: hElp [0.931, 0.907, 0.767, 0.249, 0.037, 0.024, 0.023, 0.023]
SIMU:root:word phono:  help, /hElp/, wmax = 0.9924 
SIMU:root:generated phonological form: /hElp####/
SIMU:root:fixation position(s): [0]
SIMU:root:fixation VA dispersion(s): [1.75]
```
Some results can already be understood: for instance, the model recognized that "help" is a known English word (the probability that lexical decision would answer "YES" is "ld ortho : 0.9865"). It recognized the correct letters in all 4 positions with high probability (0.931 for "h" in position 1, 0.896 for "e" in position 2, 0.86 for "l" in position 3, 0.802 for "p" in position 4). 


## 2.5. Exploring the simulation results (optional)

To begin exploring a bit more the simulation end state, several fields of the "sim" object can be explored. For instance, to get more information about the letter percept, use:
```python
letter_dist = sim.get_res("percept")
letter_dist.shape
```
Here, the "shape" command tells us that letter_dist is a numpy matrix of shape (stimulus length, size of alphabet, number of iterations). In our case, the output should be "(4, 27, 1001)". To see the probability over letters at the last iteration:
```python
print(letter_dist[:,:,-1])
```

In a similar manner, the probability distributions over words, over lexical membership, and over phonemes, can be explored with:
```python
word_dist = sim.get_res(mod="ortho", dist="word")
ld_dist = sim.get_res(mod="ortho", dist="ld")
phon_dist = sim.get_res(mod="phono", dist="percept")
```

## 2.6 Now what? I want more.

You have reached the final example provided to verify your installation of the BRAID model. If previous commands executed without error messages, you're all set!

More detailed examples are provided in notebooks ("notebooks" sub-directory). The next step, if you want to see plots of the evolution of probabilities, in the model, during visual processing, would be to follow the "demo_func.ipynb" Python notebook. 

# 3. Installation of the Python package (optional)

Installing the BRAID software as a Python package (or module as it often called) can be practical for importing it without caring about path problems. Importing a module in python is done through searching for files (or directories) with same name in a list of directories given by the variable `sys.path`. According to the [Python documentation](https://docs.python.org/3/tutorial/modules.html), `sys.path` usually contains:

- the directory containing the input script (or the current directory when no file is specified);
- `PYTHONPATH` (a list of directory names, with the same syntax as for the shell variable PATH);
- the installation-dependent default (by convention including a site-packages directory, handled by the site module).

Basically, installing the package will add the specified files to a module folder in the `site-packages` directory so that it can then be imported in any Python interpreter.

To install the BRAID package, run the following command in the root directory (`braid/`):

```
$ python setup.py install
```

The setup file that we provide is configured so that all the package files (source code and resource files) are loaded in your current Python environement. To get the file path of the package, run these lines in a python interpreter:

```python
import braidpy
print(braidpy.__file__)
```

If this is run in a Conda environment, the path will be something similar to this (of course, with forward or backward slashed depending on your system):

```
'C:\\Users\\username\\Anaconda3\\lib\\site-packages\\braid-0.0.1-py3.9.egg\\braidpy\\__init__.py'
```

To uninstall the package, you can run:

```
$ pip uninstall braid
>> Found existing installation: braid 0.0.1
>> Uninstalling braid-0.0.1:
>>  Would remove:
>>     c:\users\username\anaconda3\lib\site-packages\braid-0.0.1-py3.9.egg
>>  Proceed (y/n)? y
>>  Successfully uninstalled braid-0.0.1
```

`pip` will remove the folder that contains all the loaded files during installation. If you are not sure about the package name and do not want to remove the wrong package, it is possible to check how your installed package is named from pip's point of view by using this command:

```
$ pip freeze
```

# 4. Some more information about BRAID

## BRAID versions

This is a research software, to allow "playing with the BRAID model", in order to understand it, or to develop it for further research. Since research around the BRAID model is an ongoing project, several versions have been developed, with specific monikers such as BRAID-Learn, BRAID-Phon, BRAID-Acq, etc. Versions have been developed mostly incrementally, aiming at "retro-compatibility" (that is, old simulations with newer versions should provide similar but not identical results). Therefore, expect "overall reproductibility of results", and not "reproductibility to the n-th decimal". In the unlikely case in which an experimental effect was described in a paper, and is found to disappear with the current version, please contact us (finding out why and how would be interesting).

## Software licence

This research software is provided under the CECILL-2.1 licence; it can be roughly seen as a "French adaptation" of the GNU GPL licence. See https://opensource.org/licenses/CECILL-2.1 for details.

## Scientific resources

The model relies on "external" resources, some of which are actually included, in part or in full, in the Python project. There are two such kinds of resources:
- lexicons (databases of words, in a given language, usually with descriptors such as their length, their pronunciations, etc.),
- and confusion matrices (matrices giving the recognition probability of letters given the letter visually presented -- some of these were slightly adapted numerically by us).

We acknowledge the use of the following references, that provided these resources.

### Lexicons

- New, B., Pallier, C., Brysbaert, M., Ferrand, L. (2004) Lexique 2: A New French Lexical Database. Behavior Research Methods, Instruments, & Computers, 36 (3), 516-524. [https://doi.org/10.3758/bf03195598] (Lexique 3.82, see http://www.lexique.org/)
- Ferrand, L., New, B., Brysbaert, M., Keuleers, E., Bonin, P.,Méot, A., . . . Pallier, C. (2010). The French Lexicon Project: Lexical decision data for 38,840 French words and 38,840 pseudowords. *Behavior Research Methods*, *42*(2), 488–496. https://doi.org/10.3758/brm.42.2.488
- Balota, D. A., Yap, M. J., Hutchison, K. A., Cortese, M. J., Kessler, B., Loftis, B., Neely, J. H., Nelson, D. L., Simpson, G. B., & Treiman, R. (2007). The English Lexicon Project. *Behavior Research Methods*, *39*(3), 445–459. https://doi.org/10.3758/bf03193014
- Baayen, R. H., Piepenbrock, R., & Gulikers, L. (1995). The CELEX lexical database (release 2). *Distributed by the Linguistic Data Consortium, University of Pennsylvania*.
- Keuleers, E., Lacey, P., Rastle, K. *et al.* The British Lexicon Project: Lexical decision data for 28,730 monosyllabic and disyllabic English words. *Behavior Research Methods*  44, 287–304 (2012). https://doi.org/10.3758/s13428-011-0118-4
- Lété, B., Sprenger-Charolles, L. & Colé, P. MANULEX: A grade-level lexical database from French elementary school readers. *Behavior Research Methods, Instruments, & Computers* 36, 156–166 (2004). https://doi.org/10.3758/BF03195560
- van Heuven, W. J. B., Mandera, P., Keuleers, E., & Brysbaert, M. (2014). Subtlex-UK: A New and Improved Word Frequency Database for British English. *Quarterly Journal of Experimental Psychology*, *67*(6), 1176–1190. https://doi.org/10.1080/17470218.2013.850521

### Confusion matrices

- Townsend, J. T. (1971). Theoretical analysis of an alphabetic confusion matrix. *Perception & Psychophysics*, *9*(1), 40–50. https://doi.org/10.3758/BF03213026
- Geyer, L. H. (1977). Recognition and confusion of the lowercase alphabet. *Perception & Psychophysics*, *22*(5), 487–490. https://doi.org/10.3758/BF03199515
- Simpson, I. C., Mousikou, P., Montoya, J. M., & Defior, S. (2012). A letter visual-similarity matrix for Latin-based alphabets. *Behavior Research Methods*, *45*(2), 431–439. https://doi.org/10.3758/s13428-012-0271-4
- Paap, K. R., Newsome, S. L., McDonald, J. E., & Schvaneveldt, R. W. (1982). An activation-verification model for letter and word recognition: the word-superiority effect. *Psychological Review*, *89*(5), 573–594.
- Loomis, J.M. Analysis of tactile and visual confusion matrices. *Perception & Psychophysics* 31, 41–52 (1982). https://doi.org/10.3758/BF03206199
- Gilmore, G.C., Hersh, H., Caramazza, A. *et al.* Multidimensional letter similarity derived from recognition errors. *Perception & Psychophysics* 25, 425–431 (1979). https://doi.org/10.3758/BF03199852
- van der Heijden, A.H.C., Schreuder, R., Maris, L. *et al.* Some evidence for correlated separate activation in a simple letter-detection task. *Perception & Psychophysics* 36, 577–585 (1984). https://doi.org/10.3758/BF03207519


# Bibliographic references

- Ginestet, E., Phénix, T., Diard, J., and Valdois, S. (2019). Modeling the length effect for words in lexical decision: The role of visual attention. Vision Research, 159:10–20
- Phénix, T. (2018). Modélisation bayésienne algorithmique de la reconnaissance visuelle de mots et de l’attention visuelle. PhD thesis, Université Grenoble Alpes
- Phénix, T., Valdois, S., and Diard, J. (2018). Reconciling opposite neighborhood frequency effects in lexical decision: Evidence from a novel probabilistic model of visual word recognition. In Rogers, T., Rau, M., Zhu, X., and Kalish, C. W., editors, Proceedings of the 40th Annual Conference of the Cognitive Science Society, pages 2238–2243, Austin, TX. Cognitive Science Society
- Saghiran, A., Valdois, S., and Diard, J. (2020). Simulating length and frequency effects across multiple tasks with the Bayesian model BRAID-Phon. In Proceedings of the 42th Annual Conference of the Cognitive Science Society, pages 3158–3163, Austin, TX. Cognitive Science Society.

# Contact

Julien Diard: julien.diard@univ-grenoble-alpes.fr

