BRAID model
===========

BRAID is a computational  model of the visual processes and knowledge involved in letter recognition, word recognition and lexical decision. Probabilistic variables and probability distributions model representations and knowledge, and probabilistic inference provides mathematical expressions for simulated tasks. The BRAID model features the three classical levels of visual processing, as in the seminal IA model (Mclelland & Rumelhart, 1981): the letter sensory level, the letter perceptual level and the lexical level (see Figure). The main originality of BRAID is the inclusion of a visual attention layer, between the sensory layer and the letter perception layer. Here, we only provide a rapid description of each submodel; the full mathematical description is available in Thierry Phénix PhD thesis manuscript.

The **sensory submodel** of BRAID implements low-level visual processing, taking into account effects from an acuity gradient, from lateral interference between letters of the stimulus, and from visual similarity between letters. :math:`S^t_n` variables describe the stimulus at each time step :math:`t` and each position :math:`n`, :math:`G^t` represent the gaze position at time step t. Letter processing is parallel and results in probability distributions over variables :math:`I^t_n`, that represent internal letter representation.

The **visual-attention submodel** spatially modulates the transfer of information propagation during sensory processing, from the sensory to the perceptual submodels, to favor processing of the attended portion of the stimulus, to the detriment of the rest. In other words, it can be construed as a spatial filter of information across letter positions. The distribution of attention is represented by  a probability distribution, assumed to be Normal over spatial positions, so that it is described by its location :math:`\mu_A^t` and dispersion :math:`\sigma_A^t`.

The **letter perceptual submodel** is a dynamic model that implements accumulation of perceptual evidence. Probability distributions over variables :math:`P^t_n` evolve over time as they receive information from sensory processing, allowing to identify letters at each position :math:`n` and each time step :math:`t`. 

The **lexical knowledge submodel** represents knowledge about the orthography of known words, with a naïve Bayes model predicting, for each word W, its letter :math:`L^t_n` at each position. In the present study, the lexical submodel is configured with the (Lexique 3.82)  database, featuring the spelling of 92,117 French words (New et al., 2006). The lexical submodel  also includes a dynamical model, to accumulate  perceptual evidence  about which known word corresponds to the stimulus, if one does. The initial state of this dynamical model is the prior distribution :math:`P(W^0)`, which is identified to word frequency from the reference lexicon.

The final submodel,  the **lexical membership submodel**, allows evaluating the correspondence between the stimulus and known words, using a dynamically evolving probability distribution over  Boolean variable :math:`D^t`. In other words, this submodel implements an "error model": assuming the input string is a word (:math:`D^t = \textit{True}`),  perceived letters should match those of a known word in all positions; on the contrary, if the input is not a word (:math:`D^t = \textit{False}`), matching should fail in at least one position. 
%This submodel is useful for simulating the lexical decision task.

In the overall architecture of the BRAID model, information propagates at each time step both in a bottom-up manner (from the sensory to the perceptual to both the lexical knowledge and lexical membership submodels) and in a top-down manner (from the lexical submodels to the perceptual submodel). Therefore, lexical top-down information influences processing at the letter level, yielding word and pseudo-word superiority effects, as in classical models. Further, this influence is modulated by lexical membership: if the stimulus is probably not  a word, top-down influence is decreased so that  only perceptual, bottom-up information contributes to  letter identification probabilities. 


Here is a graphical representation of the BRAID model. Each submodel is represented by a colored, labelled rectangle. Variables are represented by nodes and arrows refer to probabilistic dependencies.

.. image:: figs/braid_gpm.png
  :width: 500
  :alt: braid_gpm


For more details:

  - `Phénix, T. (2018). Modélisation bayésienne algorithmique de la reconnaissance visuelle de mots et de l’attention visuelle. PhD thesis, Université Grenoble Alpes <https://tel.archives-ouvertes.fr/tel-02368168>`_. 

.. toctree::
   :maxdepth: 2