BRAID-Phon model
================

In the BRAID model, in addition to the lexical membership evaluation mechanism, only knowledge about the spelling of known words. There are two components to represent this lexical spelling knowledge~: first, an a priori distribution over the space of known words, :math:`P(W^0)`, which represents the frequency of words in the lexicon, and second, a set of probability distributions of the form :math:`P(L_n^t~|~[W^t=w])`, which describe knowledge about the :math:`n`-th letter of the word :math:`w` distributions, yielding very certain knowledge about that letter for that word). The model is completed by a temporal chain, based on a dynamic term :math:`P(W^t~|~W^{t-1})`, to allow the accumulation of perceptual evidence, at each instant, about the identity of the word in the stimulus.

In BRAID-Phon, we develop this lexical submodel, with two goals. Our first goal is to extend the lexical submodel to include, in a symmetrical way, knowledge about the phonological representations of words. In this work, we choose, for simplicity, that phonological representations consist of phonemic representations~: in other words, the sounds for a word are encoded by a sequence of phonemes (*e.g.*, we do not address here the question of the syllabic or phonemic nature of phonological representations). Our second goal is to implement the hypothesis of a single path between the orthographic and phonological representations of the model. Therefore, we choose the constraint of only "link" the orthographic and phonological representations through the lexicon word representation space.

In order to extend the BRAID word recognition model into the BRAID-Phon reading aloud model, we add a phonological sub-layer, extending the  lexical knowledge submodel. 
We represent the phonological description of each known word in a mirror manner to orthography: the probability distribution over phonemes  :math:`\\phi^t_{1:M}`, for all positions 1 to :math:`M`, is defined by a time-invariant naïve Bayes model. 
:math:`M` is the maximal phonological length of words in the lexicon and a specific phoneme value (#) is used to indicate the end of the phonological sequence for shorter words.

To represent phonology, we restrict to a minimalist solution by encoding only phonemes identity in a similar fashion of spelling. Other aspects of phonology are not included in the model for  simplicity and since this is a first attempt.

Mathematically, :math:`P(W^0)` is is a prior representing word frequencies extracted from lexicon. 
:math:`P(W^t~|~W^{t-1})` are transition probabilities of the dynamic  model describing evolution of words identity probabilities.
It is a memory decay model that allows maintaining and accumulating information over time. But when the system has no input, words identity probabilities converge towards the initial values :math:`P(W^0)`.


Here is a graphical representation of the BRAID Phon model. Each submodel is represented by a colored, labelled rectangle. Variables are represented by nodes and arrows refer to probabilistic dependencies.

.. image:: figs/braid-phon_gpm.png
  :width: 500
  :alt: braid-phon_gpm


.. toctree::
   :maxdepth: 2