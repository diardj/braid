.. Braid documentation master file, created by
   sphinx-quickstart on Tue Sep  7 16:33:12 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Braid's documentation!
=================================

Getting started
---------------

This menu refers to the README file of the BRAID project.
Mainly, it shows how to install BRAID, how to run a simulation with this model and some examples of use.

.. toctree::
   :maxdepth: 2

   readme

For more details 
----------------
.. toctree::
   :maxdepth: 1
   
   braidmodel
   braidphonmodel
   braidpy

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
