braidpy.utilities
=================

.. automodule:: braidpy.utilities
   
   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
      :template: custom_func_sum.rst
   
      abstractmethod
      addLoggingLevel
      attention_loss
      build_interference_weight
      build_word_transition_vector
      create_repr
      edit_distance
      fullspec
      gaussian
      gaussian_cdf
      get_size
      l_round
      ld_control
      len2phlen
      norm1D
      norm2D
      norm3D
      norm_percept
      pseudodirac
      uniform
      wsim
      wsim_mask
   
   

   
   
   

   
   
   



